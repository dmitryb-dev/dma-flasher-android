package com.barannik.events;

import java.util.LinkedList;
import java.util.List;

public class ObservableEvent<T> implements Event<T> {

    private List<Listener<T>> listeners = new LinkedList<>();

    @Override
    public Subscription subscribe(final Listener<T> listener) {
        listeners.add(listener);
        return new Subscription() {
            @Override
            public void unsubscribe() {
                listeners.remove(listener);
            }
        };
    }

    public void notifySubscribers(T observable) {
        for (Listener<T> listener: listeners) {
            listener.onEvent(observable);
        }
    }

}
