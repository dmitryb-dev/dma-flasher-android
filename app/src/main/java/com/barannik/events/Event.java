package com.barannik.events;

public interface Event<T> {
    Subscription subscribe(Listener<T> listener);
}
