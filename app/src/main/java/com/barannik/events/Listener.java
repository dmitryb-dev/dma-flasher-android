package com.barannik.events;

public interface Listener<T> {
    void onEvent(T observable);
}
