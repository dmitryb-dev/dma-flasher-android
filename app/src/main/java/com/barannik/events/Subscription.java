package com.barannik.events;

public interface Subscription {
    void unsubscribe();
}
