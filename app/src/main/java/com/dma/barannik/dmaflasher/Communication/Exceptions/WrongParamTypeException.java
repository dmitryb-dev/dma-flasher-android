package com.dma.barannik.dmaflasher.Communication.Exceptions;

import com.dma.barannik.dmaflasher.Communication.DeviceParam;

public class WrongParamTypeException extends ParamException {
    public WrongParamTypeException(DeviceParam param, String value) {
        super(param, value);
    }
}
