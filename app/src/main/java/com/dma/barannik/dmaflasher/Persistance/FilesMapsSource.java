package com.dma.barannik.dmaflasher.Persistance;

import android.content.Context;
import android.content.res.AssetManager;
import com.dma.barannik.dmaflasher.ParamsMap.DMADevice;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

public class FilesMapsSource implements MapsSource {

    public static final String MAP_EXTENSION = FileExtensionAssociation.MAP.getExtension();
    public static final String PRESET_EXTENSION = FileExtensionAssociation.PRESET.getExtension();

    private Collection<MapSummary> allDMAFiles;

    private static Collection<FileMapSummary> grabFiles(Context context) {
        return grabFiles(context.getFilesDir());
    }

    private static Collection<FileMapSummary> grabFiles(File file) {
        Collection<FileMapSummary> found = new ArrayList<>();

        if (file.isFile()) {
            if (FileUtils.isFileHasExtension(file, new String[]{MAP_EXTENSION, PRESET_EXTENSION})) {
                found.add(new FileMapSummary(file));
            }
        }
        else {
            File[] files = file.listFiles();
            for (int i = 0; i < files.length; i++)
                found.addAll(grabFiles(files[i]));
        }

        return found;
    }

    private static Collection<AssetMapSummary> grabAssets(Context context) {
        return grabAssets(context, "", true);
    }

    private static Collection<AssetMapSummary> grabAssets(Context context, String path, boolean isRoot) {
        Collection<AssetMapSummary> found = new ArrayList<>();

        try {
            String[] fileNames = context.getAssets().list(path);
            if (fileNames.length > 0) {
                for (String file: fileNames) {
                    found.addAll(grabAssets(context, path + "/" + file, false));
                }
            } else if (!isRoot && FileUtils.isFileHasExtension(path, new String[]{MAP_EXTENSION, PRESET_EXTENSION})) {
                found.add(new AssetMapSummary(path, context.getAssets()));
            }
        } catch (IOException e) {
            throw new IllegalStateException("Can't list assets");
        }
        return found;
    }

    public FilesMapsSource(Context context) {
        Collection<MapSummary> found = new ArrayList<>();
        found.addAll(grabAssets(context));
        found.addAll(grabFiles(context));
        this.allDMAFiles = found;
    }

    @Override
    public Collection<MapSummary> supportedMapsFor(DMADevice device) {
        Collection<MapSummary> supported = new LinkedList<>();
        for (MapSummary summary: allDMAFiles)
            if (summary.getTargetDevice().equals(device))
                supported.add(summary);
        //return LanguageFilter.filterByCurrentLanguage(supported);
        return supported;
    }
}
