package com.dma.barannik.dmaflasher.ParamsMap.Legacy.Serialize;

import com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.Group;
import com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.Param;
import com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.Schema;
import com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.Value;
import com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.field.Field;
import com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.field.NumberField;
import com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.field.PresetField;
import com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.field.StringField;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SchemaParserEMAP implements SchemaParser {

    @Override
    public Schema parse(String source) {
        source = removeComments(source);
        return parseSchema(source);
    }

    public Schema parseSchema(String source) {
        Schema schema = parseDescription(getHead(source));

        Map<String, PresetField> tables = new HashMap<>();
        for(String table: getTables(source))
            tables.put(parserTableName(table), parseTable(table));

        for(String page: getPages(source))
            schema.addGroup(parsePage(page, tables));

        return schema;
    }

    public Schema parseDescription(String head) {
        String[] lines = head.split("(\\r\\n|\\r|\\n)");
        Schema schema = new Schema(lines[1], lines[0]);
        String speedString = lines[2].split(":")[1];
        schema.setSpeed(Integer.parseInt(speedString));
        return schema;
    }

    public Group parsePage(String source, Map<String, PresetField> tables) {
        Group group = new Group(Regex.find(source, "(?<=(<\\S{0,400}\\s))(.+)(?=>)").get(0));
        List<String> paramStrings = Regex.find(source, "(?<=(.{0,4000}[\\r\\n])).+(?=([\\r\\n].*))");

        for (String paramString: paramStrings)
            group.addParam(parseParam(paramString, tables));

        return group;
    }

    public Param parseParam(String source, Map<String, PresetField> tables) {
        String[] paramDescriptors = source.split("\\|");
        Field field = paramDescriptors[4].equals("N")?
                parseField(paramDescriptors[2], paramDescriptors[3]) : tables.get(paramDescriptors[4].substring(1).trim()).clone();
        if (field == null) throw new RuntimeException("No table for param " + paramDescriptors[0] + " table: " + paramDescriptors[4]);
        return new Param(paramDescriptors[0], paramDescriptors[1], paramDescriptors[2],
                field, paramDescriptors[5]);
    }

    public Field parseField(String type, String boundsString) {
        String[] boundsSplit = boundsString.split(":");
        int[] bounds = { Integer.parseInt(boundsSplit[0]), Integer.parseInt(boundsSplit[1]) };

        switch (type) {
            case "str": return new StringField("", bounds[1]);
            default: return new NumberField(bounds[0], bounds[1]);
        }
    }

    public String parserTableName(String source) {
        return Regex.find(source, "(?<=(<\\S{0,200}\\s))(.+)(?=>)").get(0);
    }
    public PresetField parseTable(String source) {
        PresetField preset = new PresetField();
        preset.setId(parserTableName(source));
        List<String> valueStrings = Regex.find(source, "(?<=(.{0,10000}[\\r\\n])).+(?=([\\r\\n].{0,10000}))");

        for (String valueString: valueStrings) {
            String[] split = valueString.split("\\|");
            preset.addValue(new Value(split[0], split[1]));
        }
        return preset;
    }

    public String removeComments(String source) {
        return source.replaceAll("(\\/\\/.*)|(\\/\\*(.|[\\r\\n])*\\*\\/)", "");
    }

    public String getHead(String source) {
        return Regex.find(source, "^.*[\\r\\n]+.*[\\r\\n]+.*[\\r\\n]+.*[\\r\\n]+").get(0);
    }

    public List<String> getPages(String source) {
        return Regex.find(source, "<((P|p)(AGE|age))\\s.*>[\\S\\s]*?\\1>");
    }

    public List<String> getTables(String source) {
        return Regex.find(source, "<((T|t)(ABLE|able))\\s.*>[\\S\\s]*?\\1>");
    }
}
