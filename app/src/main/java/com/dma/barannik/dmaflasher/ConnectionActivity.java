package com.dma.barannik.dmaflasher;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.dma.barannik.dmaflasher.Communication.Connection.Connection;
import com.dma.barannik.dmaflasher.Communication.Connection.IgnoreEchoConnection;
import com.dma.barannik.dmaflasher.Communication.DeviceInfo;
import com.dma.barannik.dmaflasher.Communication.Exceptions.DeviceCommunicationException;
import com.dma.barannik.dmaflasher.Communication.Exceptions.UnsupportedDeviceException;
import com.dma.barannik.dmaflasher.Communication.Protocol.DMA2DeviceProtocol;
import com.dma.barannik.dmaflasher.ParamsMap.DMADevice;
import com.dma.barannik.dmaflasher.Persistance.DMAContext;
import com.dma.barannik.dmaflasher.Persistance.FilesMapsSource;
import com.dma.barannik.dmaflasher.Workflow.Alerts;

import java.util.HashMap;
import java.util.Map;

public class ConnectionActivity extends BTDependedActivity {

    private DMA2DeviceProtocol protocol;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection);

        requireBluetooth();
        identifyDevice();
    }

    private void identifyDevice() {
        final Map<Class<? extends Exception>, Integer> exceptionMessages =
            new HashMap<Class<?extends Exception>, Integer>() {{
                put(DeviceCommunicationException.class, R.string.not_dma_device);
                put(UnsupportedDeviceException.class, R.string.unsupported_device);
            }};
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    connect();
                    setStatus(R.string.reading_info);
                    readDeviceInfo();
                    setStatus(R.string.looking_for_files);
                    Intent mapsActivity = new Intent(ConnectionActivity.this, SelectPresetActivity.class);
                    startActivity(mapsActivity);
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Can't connect", "=(((", e);
                    DMAContext.clear();
                    if (!canceled) {
                        Integer messageId = exceptionMessages.get(e.getClass());
                        Alerts.error(
                                messageId != null? messageId : R.string.cannot_connect,
                                ConnectionActivity.this
                        );
                    }
                }
            }
        }, "Connection thread").start();
    }

    private boolean isWaitMessageEnabled = false;
    private boolean canceled = false;
    @Override
    public void onBackPressed() {
        if (isWaitMessageEnabled) {
            Alerts.errorMessage(R.string.wait, this);
            isWaitMessageEnabled = false;
        } else {
            canceled = true;
            DMAContext.clear();
            super.onBackPressed();
        }
    }

    private void setStatus(final int id) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((TextView) findViewById(R.id.status)).setText(id);
            }
        });
    }

    private void connect() throws InterruptedException {
        Log.i("Connection", "Connecting.");
        Connection connection = DMAContext.connected.connect();
        DMAContext.connection = connection;
        Log.i("Connection", "Connected. Opening.");
        connection.open();
        Log.i("Connection", "Opened.");
        protocol = new DMA2DeviceProtocol(connection);
        DMAContext.protocol = protocol;
        Thread.sleep(200);
    }

    private void readDeviceInfo() {
        DMAContext.deviceInfo = protocol.getInfo();
        DMAContext.maps = new FilesMapsSource(ConnectionActivity.this)
                .supportedMapsFor(new DMADevice(DMAContext.deviceInfo));

        if (DMAContext.maps.size() == 0) {
            throw new UnsupportedDeviceException();
        }
    }
}
