package com.dma.barannik.dmaflasher.ParamsMap.Legacy.model;

public class Value implements Cloneable {

    public Value clone() {
        return new Value(description, value);
    }

    private String description;
    private String value;

    public String getDescription() {
        return description;
    }
    public Object getValue() {
        return value;
    }

    public Value(String description, String value) {
        this.description = description;
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Value value1 = (Value) o;

        return value.equals(value1.value);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }
}
