package com.dma.barannik.dmaflasher.Persistance;

import com.dma.barannik.dmaflasher.ParamsMap.DMADevice;
import com.dma.barannik.dmaflasher.ParamsMap.ParamsMap;

import java.io.File;

public class FileMapSummary implements MapSummary {

    private String name;
    private DMADevice targetDevice;
    private File file;
    private boolean isPreset;
    private String language;

    public FileMapSummary(File file) {
        DMAMapParams dmaInfo = DMAMapParams.fromFilename(file.getName());
        this.name = dmaInfo.getName();
        this.targetDevice = new DMADevice(dmaInfo.getTargetDevice());
        this.language = dmaInfo.getLanguage();
        this.file = file;
        this.isPreset = FileUtils.getExtension(file).equals(FilesMapsSource.PRESET_EXTENSION);
    }

    @Override
    public String getLanguage() {
        return language;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public DMADevice getTargetDevice() {
        return targetDevice;
    }

    @Override
    public ParamsMap load() {
        return FileExtensionAssociation.fromExtension(FileUtils.getExtension(file))
                .getParser()
                .parse(FileUtils.readFile(file));
    }

    @Override
    public boolean isPreset() {
        return isPreset;
    }
}
