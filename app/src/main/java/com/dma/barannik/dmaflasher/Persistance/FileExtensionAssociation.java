package com.dma.barannik.dmaflasher.Persistance;

import com.dma.barannik.dmaflasher.ParamsMap.Persistance.Parsers.EMAPParser;
import com.dma.barannik.dmaflasher.ParamsMap.Persistance.Parsers.ParamsMapParser;
import com.dma.barannik.dmaflasher.ParamsMap.Persistance.Parsers.PresetParser;
import com.dma.barannik.dmaflasher.ParamsMap.Persistance.Serialize.ParamsMapToString;
import com.dma.barannik.dmaflasher.ParamsMap.Persistance.Serialize.PresetToString;

public class FileExtensionAssociation {

    public static final String
            MAP_EXTENSION = "emap",
            PRESET_EXTENSION = "dmaset";

    public static final FileExtensionAssociation
            MAP = new FileExtensionAssociation(MAP_EXTENSION, new EMAPParser(), null),
            PRESET = new FileExtensionAssociation(PRESET_EXTENSION, new PresetParser(), new PresetToString());

    private String extension;
    private ParamsMapParser parser;
    private ParamsMapToString serializer;

    public static FileExtensionAssociation fromExtension(String extension) {
        switch (extension) {
            case MAP_EXTENSION: return MAP;
            case PRESET_EXTENSION: return PRESET;
            default: return null;
        }
    }

    FileExtensionAssociation(String extension, ParamsMapParser parser, ParamsMapToString serializer) {
        this.extension = extension;
        this.parser = parser;
        this.serializer = serializer;
    }

    public String getExtension() {
        return extension;
    }

    public ParamsMapParser getParser() {
        return parser;
    }

    public ParamsMapToString getSerializer() {
        return serializer;
    }
}
