package com.dma.barannik.dmaflasher.ParamsMap;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

public class Tab implements Iterable<Param>, Serializable {
    private String name;
    private List<Param> params;

    public Tab(String name, List<Param> params) {
        this.name = name;
        this.params = params;
    }

    public String getName() {
        return name;
    }

    public List<Param> getParams() {
        return params;
    }

    @Override
    public Iterator<Param> iterator() {
        return params.iterator();
    }
}
