package com.dma.barannik.dmaflasher.ParamsMap.Persistance.Parsers;

import com.dma.barannik.dmaflasher.ParamsMap.DMADevice;
import com.dma.barannik.dmaflasher.ParamsMap.InputFIelds.InputField;
import com.dma.barannik.dmaflasher.ParamsMap.InputFIelds.IntInputField;
import com.dma.barannik.dmaflasher.ParamsMap.InputFIelds.PresetInputField;
import com.dma.barannik.dmaflasher.ParamsMap.InputFIelds.StringInputField;
import com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.Group;
import com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.Schema;
import com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.Value;
import com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.field.*;
import com.dma.barannik.dmaflasher.ParamsMap.Param;
import com.dma.barannik.dmaflasher.ParamsMap.ParamsMap;
import com.dma.barannik.dmaflasher.ParamsMap.Tab;

import java.util.ArrayList;
import java.util.List;

public class LegacySchemaToParamsMap {
    public static ParamsMap convert(Schema schema) {
        List<Tab> tabs = new ArrayList<>();
        for (Group group: schema) {
            List<Param> params = new ArrayList<>();
            for (com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.Param param: group)
                params.add(new Param(
                        param.getAddress(),
                        param.getType(),
                        (InputField) param.getField().accept(new FieldConverter(param))
                ));

            tabs.add(new Tab(group.getName(), params));
        }

        return new ParamsMap(
                schema.getDescription(),
                new DMADevice(schema.getDevice()),
                tabs);
    }

    private static class FieldConverter implements FieldVisitor {
        private com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.Param param;
        public FieldConverter(com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.Param param) {
            this.param = param;
        }

        @Override
        public Object visit(Field field) {
            throw new IllegalArgumentException("Not supported");
        }

        @Override
        public Object visit(BooleanField booleanField) {
            throw new IllegalArgumentException("Not supported");
        }

        @Override
        public Object visit(FloatNumberField floatNumberField) {
            throw new IllegalArgumentException("Not supported");
        }

        @Override
        public IntInputField visit(NumberField numberField) {
            return new IntInputField(
                    param.getName(),
                    param.getDescription(),
                    numberField.getDefaultValue().intValue(),
                    (int) numberField.getMin(),
                    (int) numberField.getMax()
            );
        }

        @Override
        public Object visit(com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.field.PresetField presetField) {
            List<PresetInputField.Value> values = new ArrayList<>();
            for (Value value: presetField)
                values.add(new PresetInputField.Value(value.getDescription(), value.getValue().toString()));
            return new PresetInputField(
                    param.getName(),
                    param.getDescription(),
                    values,
                    presetField.getDefaultField()
            );
        }

        @Override
        public StringInputField visit(StringField stringField) {
            return new StringInputField(
                    param.getName(),
                    param.getDescription(),
                    stringField.getDefaultValue(),
                    0,
                    stringField.getMaxLength()
            );
        }
    }
}
