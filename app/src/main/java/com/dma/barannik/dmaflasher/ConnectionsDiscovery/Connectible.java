package com.dma.barannik.dmaflasher.ConnectionsDiscovery;

import com.dma.barannik.dmaflasher.Communication.Connection.Connection;

import java.io.Serializable;

public interface Connectible extends Serializable {
    Connection connect();
}
