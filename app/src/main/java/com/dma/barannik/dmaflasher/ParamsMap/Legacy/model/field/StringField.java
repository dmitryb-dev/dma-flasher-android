package com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.field;

public class StringField extends FieldWithId {

    private String defaultValue;
    private int maxLength;

    public StringField() {
        defaultValue = "";
        maxLength = 256;
    }

    public StringField(String defaultValue) {
        this.defaultValue = defaultValue;
        maxLength = 256;
    }
    public StringField(String defaultValue, int maxLength) {
        this.defaultValue = defaultValue;
        this.maxLength = maxLength;
    }

    @Override
    public StringField clone() {
        return new StringField(defaultValue);
    }

    @Override
    public Object accept(FieldVisitor visitor) {
        return visitor.visit(this);
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public void setDefaultValue(Object value) {
        defaultValue = (String) value;
    }


    public int getMaxLength() {
        return maxLength;
    }
}
