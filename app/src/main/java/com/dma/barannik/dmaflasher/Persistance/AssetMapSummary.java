package com.dma.barannik.dmaflasher.Persistance;

import android.content.res.AssetManager;
import com.dma.barannik.dmaflasher.ParamsMap.DMADevice;
import com.dma.barannik.dmaflasher.ParamsMap.ParamsMap;

import java.io.File;
import java.io.IOException;

public class AssetMapSummary implements MapSummary {

    private String name;
    private AssetManager manager;
    private String assetName;
    private DMADevice targetDevice;
    private boolean isPreset;
    private String language;

    public AssetMapSummary(String path, AssetManager manager) {
        this.manager = manager;
        this.assetName = path.substring(path.lastIndexOf('/') + 1, path.length());
        DMAMapParams dmaInfo = DMAMapParams.fromFilename(assetName);
        this.name = dmaInfo.getName();
        this.language = dmaInfo.getLanguage();
        this.targetDevice = new DMADevice(dmaInfo.getTargetDevice());
        this.isPreset = FileUtils.getExtension(assetName).equals(FilesMapsSource.PRESET_EXTENSION);
    }

    @Override
    public String getLanguage() {
        return language;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public DMADevice getTargetDevice() {
        return targetDevice;
    }

    @Override
    public ParamsMap load() {
        try {
            return FileExtensionAssociation.fromExtension(FileUtils.getExtension(assetName))
                    .getParser()
                    .parse(FileUtils.readStream(manager.open(assetName)));
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Can't read asset '%s'", assetName), e);
        }
    }

    @Override
    public boolean isPreset() {
        return isPreset;
    }
}
