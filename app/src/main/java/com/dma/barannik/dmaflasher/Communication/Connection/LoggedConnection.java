package com.dma.barannik.dmaflasher.Communication.Connection;

import android.util.Log;

import com.dma.barannik.dmaflasher.Communication.Exceptions.DeviceDisconnectedException;

public class LoggedConnection implements Connection {

    private final String LOG_TAG = "Connection";
    private Connection decorated;

    public LoggedConnection(Connection decorated) {
        this.decorated = decorated;
    }

    @Override
    public void open() throws DeviceDisconnectedException {
        decorated.open();
    }

    @Override
    public void close() throws DeviceDisconnectedException {
        decorated.close();
    }

    @Override
    public void send(String data) throws DeviceDisconnectedException {
        Log.i(LOG_TAG, "Sending data: " + data);
        decorated.send(data);
        Log.i(LOG_TAG, String.format("Successfully sent %s chars", data.length()));
    }

    @Override
    public String read(int lines) throws DeviceDisconnectedException {
        Log.i(LOG_TAG, String.format("Reading %s lines", lines));
        String received = decorated.read(lines);
        Log.i(LOG_TAG, "Read data: " + received);
        return received;
    }
}
