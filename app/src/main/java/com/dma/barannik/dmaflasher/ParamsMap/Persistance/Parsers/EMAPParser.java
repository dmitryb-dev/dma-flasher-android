package com.dma.barannik.dmaflasher.ParamsMap.Persistance.Parsers;

import com.dma.barannik.dmaflasher.ParamsMap.Legacy.Serialize.SchemaParserEMAP;
import com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.Schema;
import com.dma.barannik.dmaflasher.ParamsMap.ParamsMap;
import com.dma.barannik.dmaflasher.Persistance.FileMapSummary;
import com.dma.barannik.dmaflasher.Persistance.MapSummary;

public class EMAPParser implements ParamsMapParser {
    @Override
    public ParamsMap parse(String source) {
        return LegacySchemaToParamsMap.convert(new SchemaParserEMAP().parse(source));
    }
}
