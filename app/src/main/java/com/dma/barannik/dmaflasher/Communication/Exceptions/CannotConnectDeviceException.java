package com.dma.barannik.dmaflasher.Communication.Exceptions;

public class CannotConnectDeviceException extends DeviceCommunicationException {
    public CannotConnectDeviceException() {
    }

    public CannotConnectDeviceException(String detailMessage) {
        super(detailMessage);
    }

    public CannotConnectDeviceException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public CannotConnectDeviceException(Throwable throwable) {
        super(throwable);
    }
}
