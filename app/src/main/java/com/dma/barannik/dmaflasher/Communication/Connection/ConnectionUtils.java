package com.dma.barannik.dmaflasher.Communication.Connection;

public class ConnectionUtils {
    public static int findLastOccurence(byte[] source, byte[] expectedSubArray, int times) {
        int searchStartPosition = 0;
        int lastFoundPosition = -1;
        for (int i = 0; i < times; i++) {
            lastFoundPosition = findSubArray(source, expectedSubArray, searchStartPosition);
            if (lastFoundPosition < 0) return -1;
            searchStartPosition = lastFoundPosition + 1;
        }
        return lastFoundPosition;
    }

    public static int findSubArray(byte[] source, byte[] expectedSubArray, int startPosition) {
        for (int position = startPosition; position <= source.length - expectedSubArray.length; position++) {
            if (isSubArrayEquals(source, position, expectedSubArray))
                return position;
        }
        return -1;
    }

    public static byte[] subArray(byte[] source, int length) {
        byte[] requested = new byte[length];
        for (int i = 0; i < length; i++) {
            requested[i] = source[i];
        }
        return requested;
    }

    private static boolean isSubArrayEquals(byte[] source, int subArrayPosition, byte[] expectedSubArray) {
        for (int i = 0; i < expectedSubArray.length; i++)
            if (source[subArrayPosition + i] != expectedSubArray[i])
                return false;
        return true;
    }
}
