package com.dma.barannik.dmaflasher.Communication.Protocol;

import com.dma.barannik.dmaflasher.Communication.Connection.Connection;
import com.dma.barannik.dmaflasher.Communication.DeviceInfo;
import com.dma.barannik.dmaflasher.Communication.DeviceParam;
import com.dma.barannik.dmaflasher.Communication.Exceptions.DeviceCommunicationException;
import com.dma.barannik.dmaflasher.Communication.Exceptions.DeviceInternalErrorException;
import com.dma.barannik.dmaflasher.Communication.Exceptions.WrongParamAddressException;
import com.dma.barannik.dmaflasher.Communication.Exceptions.WrongParamException;
import com.dma.barannik.dmaflasher.Communication.Exceptions.WrongParamTypeException;
import com.dma.barannik.dmaflasher.Communication.RequestResponse.LoggedRequestResponse;
import com.dma.barannik.dmaflasher.Communication.RequestResponse.RequestResponse;
import com.dma.barannik.dmaflasher.Communication.RequestResponse.RequestResponseConnection;
import com.dma.barannik.dmaflasher.Communication.RequestResponse.SynchronizedRequestResponse;

public class DMA2DeviceProtocol implements DeviceProtocol {

    private final int
            LINES_NUMBER_OF_READ_WRITE = 1,
            LINES_NUMBER_OF_INFO = 2;

    private RequestResponse connection;

    private static final String
            ERROR = "#ERR",
            ADDRESS_ERROR = "#ERR ADRES",
            TYPE_ERROR = "#ERR TYPE",
            PARAMETER_ERROR = "#ERR PARAMETR";

    public DMA2DeviceProtocol(Connection connection) {
        this.connection =
                new LoggedRequestResponse(
                    new SynchronizedRequestResponse(
                        new RequestResponseConnection(connection)
                    )
                );
    }

    @Override
    public String getValue(DeviceParam param) throws DeviceCommunicationException {
        String answer = connection.send(
                String.format("AT+RD=%s,%s", param.getAddress(), param.getType()),
                LINES_NUMBER_OF_READ_WRITE);
        if (answer == null) {
            throw new DeviceInternalErrorException();
        }
        if (answer.startsWith(ERROR))
            switch (answer) {
                case ADDRESS_ERROR: throw new WrongParamAddressException(param, "?");
                case TYPE_ERROR: throw new WrongParamTypeException(param, "?");
            }
        return answer;
    }

    @Override
    public void set(DeviceParam param, String value) throws DeviceCommunicationException {
        String answer = connection.send(
                String.format("AT+WR=%s,%s,%s", param.getAddress(), param.getType(), value.toString()),
                LINES_NUMBER_OF_READ_WRITE);
        if (answer == null) {
            throw new DeviceInternalErrorException();
        }
        if (answer.startsWith(ERROR))
            switch (answer) {
                case ADDRESS_ERROR: throw new WrongParamAddressException(param, value);
                case TYPE_ERROR: throw new WrongParamTypeException(param, value);
                case PARAMETER_ERROR: throw new WrongParamException(param, value);
            }
    }

    @Override
    public DeviceInfo getInfo() throws DeviceCommunicationException {
        return new DeviceInfo(
                connection.send("ATI", LINES_NUMBER_OF_INFO).split("\n")[0],
                connection.send("ATI1", LINES_NUMBER_OF_INFO).split("\n")[0]
        );
    }
}
