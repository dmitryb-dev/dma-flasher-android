package com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.field;

public interface FieldVisitor {

    Object visit(Field field);
    Object visit(BooleanField booleanField);
    Object visit(FloatNumberField floatNumberField);
    Object visit(NumberField numberField);
    Object visit(PresetField presetField);
    Object visit(StringField stringField);

}
