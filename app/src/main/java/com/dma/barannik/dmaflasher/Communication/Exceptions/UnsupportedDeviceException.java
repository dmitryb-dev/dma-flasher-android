package com.dma.barannik.dmaflasher.Communication.Exceptions;

public class UnsupportedDeviceException extends RuntimeException {
    public UnsupportedDeviceException() {
    }

    public UnsupportedDeviceException(String detailMessage) {
        super(detailMessage);
    }

    public UnsupportedDeviceException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public UnsupportedDeviceException(Throwable throwable) {
        super(throwable);
    }
}
