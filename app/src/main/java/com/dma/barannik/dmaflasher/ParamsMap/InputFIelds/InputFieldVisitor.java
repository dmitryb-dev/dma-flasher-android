package com.dma.barannik.dmaflasher.ParamsMap.InputFIelds;

public interface InputFieldVisitor<T> {
    T visit(IntInputField intInputField);
    T visit(StringInputField stringInputField);
    T visit(PresetInputField presetField);
}
