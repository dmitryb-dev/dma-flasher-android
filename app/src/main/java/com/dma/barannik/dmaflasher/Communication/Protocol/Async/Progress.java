package com.dma.barannik.dmaflasher.Communication.Protocol.Async;

public class Progress {
    private int value, max;

    public Progress(int max) {
        this.max = max;
    }

    public Progress(int value, int max) {
        this.value = value;
        this.max = max;
    }

    public Progress update(int value) {
        return new Progress(value, max);
    }

    public int getValue() {
        return value;
    }

    public int getMax() {
        return max;
    }

    public float getPercent() {
        return (float) value / max;
    }
}
