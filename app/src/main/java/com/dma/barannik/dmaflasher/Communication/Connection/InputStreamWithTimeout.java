package com.dma.barannik.dmaflasher.Communication.Connection;

import android.support.annotation.NonNull;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.atomic.AtomicBoolean;

public class InputStreamWithTimeout extends InputStream {

    public static class TimeoutException extends IOException {}

    private final InputStream decorated;
    private final long timeout;
    private AtomicBoolean isClosed = new AtomicBoolean(false);

    public InputStreamWithTimeout(InputStream decorated, long timeout) {
        this.decorated = decorated;
        this.timeout = timeout;
    }

    @Override
    public int read() throws IOException {
        return decorated.read();
    }

    @Override
    public int read(@NonNull byte[] b, int off, int len) throws IOException {
        long timeoutPoint = System.currentTimeMillis() + timeout;
        while (true) {
            if (isClosed.get()) {
                throw new IOException();
            }
            if (System.currentTimeMillis() > timeoutPoint) {
                throw new TimeoutException();
            }
            if (decorated.available() > 0) {
                return super.read(b, off, decorated.available());
            }
        }
    }

    @Override
    public void close() throws IOException {
        isClosed.set(true);
        super.close();
    }
}

