package com.dma.barannik.dmaflasher.Communication.Connection;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface Socket {
    InputStream getInputStream() throws IOException;
    OutputStream getOutputStream() throws IOException;
    void open() throws IOException;
    void close() throws IOException;
}
