package com.dma.barannik.dmaflasher;

import android.bluetooth.BluetoothDevice;

import java.util.UUID;

public interface Constants {
    String DEVICE_EXTRA = BluetoothActivity.class.getCanonicalName() + BluetoothDevice.class.getName();
    UUID appUUID = UUID.fromString("ed37ecf9-6cb8-4888-baab-9e803b18ea3e");
}
