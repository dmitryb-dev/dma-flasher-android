package com.dma.barannik.dmaflasher.ParamsMap;

import com.dma.barannik.dmaflasher.Communication.DeviceParam;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ParamsMap implements Iterable<Tab> {
    private String name;
    private DMADevice targetDevice;
    private List<Tab> tabs;

    public ParamsMap(String name, DMADevice targetDevice, List<Tab> tabs) {
        this.name = name;
        this.targetDevice = targetDevice;
        this.tabs = tabs;
    }

    public String getName() {
        return name;
    }

    public DMADevice getTargetDevice() {
        return targetDevice;
    }

    public List<Tab> getTabs() {
        return tabs;
    }

    public List<DeviceParam> getDeclaredParams() {
        List<DeviceParam> params = new LinkedList<>();
        for (Tab tab: tabs) {
            for (Param param: tab) {
                params.add(new DeviceParam(param.getAddress(), param.getType()));
            }
        }
        return params;
    }

    @Override
    public Iterator<Tab> iterator() {
        return tabs.iterator();
    }
}
