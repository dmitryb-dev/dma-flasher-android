package com.dma.barannik.dmaflasher;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.barannik.dmaflasher.ParamsMap.Persistance.Serialize.PresetToString;
import com.dma.barannik.dmaflasher.Persistance.DMAContext;
import com.dma.barannik.dmaflasher.Workflow.Alerts;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

public class SaveActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save);

        List<File> userFiles = getUserFiles();
        final LinearLayout saved = (LinearLayout) findViewById(R.id.presets);
        for (final File f: userFiles) {
            final View v = getLayoutInflater().inflate(R.layout.save_maps_summary, null);
            v.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Alerts.question(R.string.really_delete, R.string.delete, SaveActivity.this, new Runnable() {
                        @Override
                        public void run() {
                            f.delete();
                            saved.removeView(v);
                            DMAContext.needsToRefresh = true;
                        }
                    });
                }
            });
            ((TextView) v.findViewById(R.id.mapName)).setText(f.getName().split("__")[1]);
            saved.addView(v);
        }

        findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveWithExistenceCheck(((EditText) findViewById(R.id.filename)).getText().toString());
            }
        });
    }

    public List<File> getUserFiles() {
        List files = new ArrayList<>();
        for (File f: getFilesDir().listFiles()) {
            if (f.isFile() &&
                    f.getName().endsWith("__user.dmaset") &&
                    f.getName().startsWith(DMAContext.deviceInfo.getName())) {
                files.add(f);
            }
        }
        return files;
    }

    public void saveWithExistenceCheck(final String filename) {
        if (filename.trim().length() == 0) {
            Toast.makeText(this, R.string.error_empty_name, Toast.LENGTH_SHORT).show();
            return;
        }
        witHExistenceCheck(filename, new SaveAction() {
            @Override
            public void save(String filename) {
                SaveActivity.this.save(filename);
            }
        });
    }

    public void witHExistenceCheck(final String _filename, final SaveAction saveAction) {
        final String filename = _filename.replaceAll("__", "_");
        for (final File f: getUserFiles()) {
            if (f.getName().contains("__" + filename + "__")) {
                Alerts.question(R.string.exist, R.string.replace, this, new Runnable() {
                    @Override
                    public void run() {
                        f.delete();
                        saveAction.save(filename);
                    }
                });
                return;
            }
        }
        saveAction.save(filename);
    }

    public void save(String filename) {
        String string = new PresetToString().toString(DMAContext.loadedMap);
        FileOutputStream outputStream;

        try {
            outputStream = openFileOutput(DMAContext.deviceInfo.getName() + "__" + filename + "__user.dmaset", Context.MODE_PRIVATE);
            outputStream.write(string.getBytes("windows-1251"));
            outputStream.close();
            DMAContext.needsToRefresh = true;
            finish();
        } catch (Exception e) {
            Alerts.errorMessage(R.string.cannot_save, this);
            return;
        }
    }

    public interface SaveAction {
        void save(String filename);
    }

}
