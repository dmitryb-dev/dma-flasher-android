package com.dma.barannik.dmaflasher.Communication.Protocol.Async;

import com.dma.barannik.dmaflasher.Communication.DeviceParam;
import com.dma.barannik.dmaflasher.Communication.Protocol.DeviceProtocol;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

public class DMAParamsReader extends StepByStepAsyncOperation {

    private final DeviceProtocol protocol;
    private Iterator<DeviceParam> params;
    private int valuesCount;
    private Map<DeviceParam, String> read = new ConcurrentHashMap<>();

    public DMAParamsReader(DeviceProtocol protocol, Collection<DeviceParam> params) {
        this.protocol = protocol;
        TreeSet<DeviceParam> paramsSet = new TreeSet<>(new Comparator<DeviceParam>() {
            @Override
            public int compare(DeviceParam lhs, DeviceParam rhs) {
                return lhs.getAddress().compareTo(rhs.getAddress());
            }
        });
        paramsSet.addAll(params);
        this.params =  paramsSet.iterator();
        valuesCount = paramsSet.size();
    }

    @Override
    public int getStepsCount() {
        return valuesCount;
    }

    @Override
    public void onStep(int i) {
        if (!params.hasNext()) {
            return;
        }
        synchronized (protocol) {
            DeviceParam param = params.next();
            read.put(param, protocol.getValue(param));
        }
    }

    public Map<DeviceParam, String> getResult() {
        return read;
    }
}
