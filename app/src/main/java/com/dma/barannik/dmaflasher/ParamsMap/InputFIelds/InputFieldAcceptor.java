package com.dma.barannik.dmaflasher.ParamsMap.InputFIelds;

public class InputFieldAcceptor {
    public static <T> T accept(InputField field, InputFieldVisitor<T> visitor) {
        if (field instanceof IntInputField) return visitor.visit((IntInputField) field);
        if (field instanceof StringInputField) return visitor.visit((StringInputField) field);
        if (field instanceof PresetInputField) return visitor.visit((PresetInputField) field);
        throw new IllegalArgumentException("Type " + field.getClass() + " is not supports now");
    }
}
