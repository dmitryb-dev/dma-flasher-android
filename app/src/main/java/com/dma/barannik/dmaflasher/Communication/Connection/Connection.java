package com.dma.barannik.dmaflasher.Communication.Connection;

import com.dma.barannik.dmaflasher.Communication.Exceptions.DeviceDisconnectedException;

public interface Connection {
    void open() throws DeviceDisconnectedException;
    void close() throws DeviceDisconnectedException;
    void send(String data) throws DeviceDisconnectedException;
    String read(int lines) throws DeviceDisconnectedException;
}
