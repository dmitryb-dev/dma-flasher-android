package com.dma.barannik.dmaflasher.View;

import android.view.View;
import com.dma.barannik.dmaflasher.ParamsMap.InputFIelds.InputField;
import com.dma.barannik.dmaflasher.Persistance.DMAContext;

import java.io.Serializable;

public abstract class AndroidParamView implements ParamView, Serializable {
    private View view;
    private InputField field;

    public AndroidParamView(View view, InputField field) {
        this.view = view;
        this.field = field;
    }

    @Override
    public void setValue(String value) throws UnsupportedValueException {
        field.setDefaultValue(value);
    }

    public View getView() {
        return view;
    }
}
