package com.dma.barannik.dmaflasher.Communication;

public class DeviceInfo {
    private String name, version;

    public DeviceInfo(String name, String version) {
        this.name = name;
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public String getVersion() {
        return version;
    }

    @Override
    public String toString() {
        return name + ": " + version;
    }
}
