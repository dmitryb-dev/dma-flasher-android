package com.dma.barannik.dmaflasher.ConnectionsDiscovery;

import com.barannik.events.Event;

public interface ConnectionDiscovery {
    Event<? extends Connectible> onFound();
    void start();
    void stop();
}
