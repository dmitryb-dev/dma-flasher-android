package com.dma.barannik.dmaflasher.Communication.RequestResponse;

import com.dma.barannik.dmaflasher.Communication.Connection.Connection;
import com.dma.barannik.dmaflasher.Communication.Exceptions.DeviceDisconnectedException;

public class SynchronizedRequestResponse implements RequestResponse{

    private RequestResponse decorated;

    public SynchronizedRequestResponse(RequestResponse decorated) {
        this.decorated = decorated;
    }

    public synchronized String send(String request, int linesOfResponse) throws DeviceDisconnectedException {
        return decorated.send(request, linesOfResponse);
    }
}
