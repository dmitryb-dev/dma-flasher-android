package com.dma.barannik.dmaflasher.Communication.RequestResponse;

import android.util.Log;

import com.dma.barannik.dmaflasher.Communication.Exceptions.DeviceDisconnectedException;

public class LoggedRequestResponse implements RequestResponse {

    private final String LOG_TAG = "ReqRes";
    private RequestResponse decorated;

    public LoggedRequestResponse(RequestResponse decorated) {
        this.decorated = decorated;
    }

    @Override
    public String send(String request, int linesOfRequest) throws DeviceDisconnectedException {
        Log.i(LOG_TAG, "Request: " + request);
        String received = decorated.send(request, linesOfRequest);
        Log.i(LOG_TAG, "Response: " + received);
        Log.i(LOG_TAG, "---");
        return received;
    }
}
