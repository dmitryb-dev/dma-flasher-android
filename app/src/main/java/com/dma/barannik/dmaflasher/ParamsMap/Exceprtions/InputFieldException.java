package com.dma.barannik.dmaflasher.ParamsMap.Exceprtions;

public class InputFieldException extends RuntimeException {
    public InputFieldException() {
    }

    public InputFieldException(String detailMessage) {
        super(detailMessage);
    }

    public InputFieldException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public InputFieldException(Throwable throwable) {
        super(throwable);
    }
}
