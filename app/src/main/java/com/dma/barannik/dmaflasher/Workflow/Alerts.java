package com.dma.barannik.dmaflasher.Workflow;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import com.dma.barannik.dmaflasher.R;

import java.util.LinkedList;
import java.util.List;

public class Alerts {
    public interface Fixer {
        void fix();
    }

    public static void fatal(int messageResource, final Activity activity) {
        new AlertDialog.Builder(activity)
                .setTitle(messageResource)
                .setNegativeButton(R.string.exit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        exit(activity);
                    }
                })
                .show();
    }

    public static void question(int messageResource, int actionResource, final Activity activity, final Runnable action) {
        new AlertDialog.Builder(activity)
                .setTitle(messageResource)
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setPositiveButton(actionResource, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        action.run();
                    }
                })
                .show();
    }

    public static void fixOrExit(int messageResource, int fixMessage, final Activity activity, final Fixer fixer) {
        new AlertDialog.Builder(activity)
                .setTitle(messageResource)
                .setNegativeButton(R.string.exit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        exit(activity);
                    }
                })
                .setPositiveButton(fixMessage, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        fixer.fix();
                    }
                })
                .show();
    }

    public static void errorMessage(final int messageResource, final Activity activity) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity, messageResource, Toast.LENGTH_LONG).show();
            }
        });
    }
    public static void errorMessage(final String message, final Activity activity) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    public static void error(int messageResource, final Activity activity) {
        errorMessage(messageResource, activity);
        activity.finish();
    }

    private static void exit(Activity activity) {
        activity.moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
    }
}
