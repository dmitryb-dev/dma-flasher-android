package com.dma.barannik.dmaflasher.Communication;

import com.dma.barannik.dmaflasher.ParamsMap.Param;

import java.io.Serializable;

public class DeviceParam implements Serializable {
    private String address, type;

    public DeviceParam(String address, String type) {
        this.address = address;
        this.type = type;
    }

    public DeviceParam(Param param) {
        address = param.getAddress();
        type = param.getType();
    }

    public String getAddress() {
        return address;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "DeviceParam{" +
                "address='" + address + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeviceParam that = (DeviceParam) o;

        if (!address.equals(that.address)) return false;
        return type.equals(that.type);

    }

    @Override
    public int hashCode() {
        int result = address.hashCode();
        result = 31 * result + type.hashCode();
        return result;
    }
}
