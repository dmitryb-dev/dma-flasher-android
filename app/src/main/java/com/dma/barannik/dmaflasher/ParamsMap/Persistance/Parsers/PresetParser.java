package com.dma.barannik.dmaflasher.ParamsMap.Persistance.Parsers;

import com.dma.barannik.dmaflasher.ParamsMap.Legacy.Serialize.SchemaParserJSON;
import com.dma.barannik.dmaflasher.ParamsMap.ParamsMap;

public class PresetParser implements ParamsMapParser {
    @Override
    public ParamsMap parse(String source) {
        return LegacySchemaToParamsMap.convert(new SchemaParserJSON().parse(source));
    }
}
