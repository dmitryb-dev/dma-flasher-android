package com.dma.barannik.dmaflasher.ParamsMap.Exceprtions;

public class WrongDefaultValueException extends InputFieldException {
    public WrongDefaultValueException() {
    }

    public WrongDefaultValueException(String detailMessage) {
        super(detailMessage);
    }

    public WrongDefaultValueException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public WrongDefaultValueException(Throwable throwable) {
        super(throwable);
    }
}
