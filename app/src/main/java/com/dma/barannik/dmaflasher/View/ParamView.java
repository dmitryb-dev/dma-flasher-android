package com.dma.barannik.dmaflasher.View;

public interface ParamView {
    String getValue();
    void setValue(String value) throws UnsupportedValueException;
}
