package com.dma.barannik.dmaflasher.Communication.Protocol.Async;

import com.barannik.events.Event;

public interface LongAsyncOperation {
    Event<LongAsyncOperation> onStart();
    Event<LongAsyncOperation> onCancel();
    Event<Exception> onFail();
    Event<LongAsyncOperation> onSuccess();
    Event<Progress> onProgress();

    void subscribeToAllEvents(AsyncOperationListener listener);

    void start();
    void cancel();
}
