package com.dma.barannik.dmaflasher.Communication.Connection;

import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class SocketBluetoothSocketAdapter implements Socket {

    private final BluetoothSocket socket;

    public SocketBluetoothSocketAdapter(BluetoothSocket socket) {
        this.socket = socket;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return socket.getInputStream();
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        return socket.getOutputStream();
    }

    @Override
    public void open() throws IOException {
        if (!socket.isConnected())
            socket.connect();
    }

    @Override
    public void close() throws IOException {
    //    if (socket.isConnected())
            socket.close();
    }
}
