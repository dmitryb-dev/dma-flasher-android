package com.dma.barannik.dmaflasher.View;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dma.barannik.dmaflasher.Communication.DeviceParam;
import com.dma.barannik.dmaflasher.ParamsMap.InputFIelds.InputFieldAcceptor;
import com.dma.barannik.dmaflasher.ParamsMap.InputFIelds.PresetInputField;
import com.dma.barannik.dmaflasher.ParamsMap.Param;
import com.dma.barannik.dmaflasher.ParamsMap.ParamsMap;
import com.dma.barannik.dmaflasher.ParamsMap.Tab;
import com.dma.barannik.dmaflasher.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AndroidMapView extends FragmentPagerAdapter implements DeviceValues {

    private ParamsMap paramsMap;
    private ViewFactoryInputFieldVisitor viewFactory;
    private ViewPager viewPager;
    private List<TabFragment> preInitializedFragments;
    private Map<DeviceParam, String> currentValues = new HashMap<>();

    public AndroidMapView(Activity activity, FragmentManager fragmentManager, ViewGroup tabs, ParamsMap map) {
        super(fragmentManager);
        paramsMap = map;
        viewFactory = new ViewFactoryInputFieldVisitor(activity);
        preInitializedFragments = initViews();
        viewPager = (ViewPager) activity.getLayoutInflater().inflate(R.layout.view_pager, null);
        viewPager.setId(View.generateViewId());
        viewPager.setAdapter(this);
        tabs.addView(viewPager);
    }

    private List<TabFragment> initViews() {
        List<TabFragment> fragments = new ArrayList<>();
        for (Tab tab: paramsMap.getTabs()) {
            for (Param p : tab) {
                if (p.getField() instanceof PresetInputField) {
                    PresetInputField presetField = (PresetInputField) p.getField();
                    this.currentValues.put(
                            new DeviceParam(p.getAddress(), p.getType()),
                            presetField.getValue(presetField.getDefaultValue()).getValue()
                    );
                } else {
                    this.currentValues.put(
                            new DeviceParam(p.getAddress(), p.getType()),
                            p.getField().getDefaultValue().toString()
                    );
                }
            }
            TabFragment fragment = new TabFragment();
            fragment.init(tab, this);
            fragments.add(fragment);
        }
        return fragments;
    }

    private Map<DeviceParam, ParamView> getAllFields() {
        Map<DeviceParam, ParamView> params = new HashMap<>();
        for (TabFragment fragment: preInitializedFragments) {
            params.putAll(fragment.getViews());
        }
        return params;
    }

    @Override
    public void setValues(Map<DeviceParam, String> values) {
        this.currentValues = values;
        for (Map.Entry<DeviceParam, String> paramValue: values.entrySet()) {
            ParamView existedView = getAllFields().get(paramValue.getKey());
            if (existedView != null) {
                existedView.setValue(paramValue.getValue());
            } else {
                Log.i(AndroidMapView.class.getName(),
                        String.format("Lost value: %s %s", paramValue.getValue(), paramValue.getKey()));
            }
        }
    }

    @Override
    public Map<DeviceParam, String> getValues() {
        Map<DeviceParam, String> values = new HashMap<>(this.currentValues);
        for (Map.Entry<DeviceParam, ParamView> paramView: getAllFields().entrySet()) {
            ParamView view = paramView.getValue();
            values.put(paramView.getKey(), view.getValue());
        }
        return values;
    }

    public void invalidate() {
        for (Map.Entry<DeviceParam, ParamView> paramView: getAllFields().entrySet()) {
            paramView.getValue().setValue(paramView.getValue().getValue());
        }
    }

    public static class TabFragment extends Fragment {
        private AndroidMapView parent;
        private Tab tab;
        private HashMap<DeviceParam, AndroidParamView> views = new HashMap<>();

        public TabFragment() {}
;
        public void init(Tab tab, AndroidMapView parent) {
            this.parent = parent;
            setRetainInstance(true);
            this.tab = tab;
            Bundle bundle = new Bundle();
            bundle.putSerializable("tab", tab);
            setArguments(bundle);
        }

        public HashMap<DeviceParam, AndroidParamView> getViews() {
            return views;
        }

        private View saved;

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            Log.d("create", "fragment");
            if (saved == null) {
                Log.d("create", "init");
                views = new HashMap<>();
                ViewGroup root = (ViewGroup) inflater.inflate(R.layout.map_page, null);
                root.setId(View.generateViewId());
                ViewGroup paramsList = (ViewGroup) root.findViewById(R.id.root);
                paramsList.setId(View.generateViewId());
                ViewFactoryInputFieldVisitor factory = new ViewFactoryInputFieldVisitor(root.getContext());
                for (Param param : tab) {
                    paramsList.addView(getNameViewFor(root.getContext(), param));
                    AndroidParamView paramView = InputFieldAcceptor.accept(param.getField(), factory);
                    View view = paramView.getView();
                    view.setId(View.generateViewId());
                    paramsList.addView(view);
                    views.put(new DeviceParam(param), paramView);
                }
                saved = root;
            }
            for (Param param : tab) {
                DeviceParam deviceParam = new DeviceParam(param);
                String value = parent.currentValues.get(deviceParam);
                AndroidParamView view = views.get(deviceParam);
                view.setValue(value);
            }
            return saved;
        }
    }

    @Override
    public Fragment getItem(int position) {
        return preInitializedFragments.get(position);
    }

    @Override
    public int getCount() {
        return paramsMap.getTabs().size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return paramsMap.getTabs().get(position).getName();
    }

    private static TextView getNameViewFor(Context context, Param param) {
        TextView view = new TextView(context);
        view.setText(param.getField().getName() + ":");
        return view;
    }

    public ViewPager getViewPager() {
        return viewPager;
    }
}
