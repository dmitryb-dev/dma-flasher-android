package com.dma.barannik.dmaflasher;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.barannik.events.Listener;
import com.dma.barannik.dmaflasher.ConnectionsDiscovery.BluetoothConnectionDiscovery;
import com.dma.barannik.dmaflasher.ConnectionsDiscovery.DMABluetoothConnectible;
import com.dma.barannik.dmaflasher.Persistance.DMAContext;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class BluetoothActivity extends BTDependedActivity {

    private BluetoothConnectionDiscovery connectionDiscovery;
    private LinearLayout paired, found;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
           // getSup.setTitle("sdfsf");
        } catch (Exception e) {}
        setContentView(R.layout.activity_bluetooth);

         if (!requireBluetooth()) {
             return;
         }

        paired = (LinearLayout) findViewById(R.id.found);
        found = (LinearLayout) findViewById(R.id.found);

        for (BluetoothDevice device: getBluetoothAdapter().getBondedDevices()) {
            addDeviceInLayout(new DMABluetoothConnectible(device), paired);
        }

        (connectionDiscovery = new BluetoothConnectionDiscovery(getApplicationContext(), getBluetoothAdapter()))
                .onFound().subscribe(new Listener<DMABluetoothConnectible>() {
            @Override
            public void onEvent(DMABluetoothConnectible connectible) {
                addDeviceInLayout(connectible, found);
            }
        });

        findViewById(R.id.findDevicesBt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFindBtClick((Button) v);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (discoveryStarted) {
            connectionDiscovery.stop();
        }
    }

    private boolean discoveryStarted = false;
    private void onFindBtClick(Button button) {
        if (!discoveryStarted) {
            if (!requireBluetooth()) {
                return;
            }
            found.removeAllViews();
            connectionDiscovery.start();
        }
        else {
            connectionDiscovery.stop();
        }
        discoveryStarted = !discoveryStarted;
        button.setText(discoveryStarted? R.string.stop_discovery : R.string.start_discovery);
    }

    private void addDeviceInLayout(final DMABluetoothConnectible connectible, ViewGroup target) {
        View deviceSummary = getLayoutInflater().inflate(R.layout.bt_device_summary_card, null);
        ((TextView) deviceSummary.findViewById(R.id.deviceName)).setText(
                connectible.getDevice().getName() != null ?
                        connectible.getDevice().getName() : connectible.getDevice().getAddress()
        );
        if (connectible.getDevice().getBondState() == BluetoothDevice.BOND_BONDED) {
            ((TextView) deviceSummary.findViewById(R.id.comment)).setText(R.string.paired);
        }
        deviceSummary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requireBluetooth();

                DMAContext.connected = connectible;

                if (connectible.getDevice().getBondState() != BluetoothDevice.BOND_BONDED) {
                    try {
                        connectible.getDevice().getClass().getMethod("createBond", (Class[]) null)
                            .invoke(connectible.getDevice(), (Object[]) null);
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }

                Intent connectionActivityIntent = new Intent(BluetoothActivity.this, ConnectionActivity.class);
                startActivity(connectionActivityIntent);
            }
        });
        target.addView(deviceSummary);
    }

    @Override
    protected void onPermissionsFail() {
        findViewById(R.id.findDevicesBt).setEnabled(false);
    }
}
