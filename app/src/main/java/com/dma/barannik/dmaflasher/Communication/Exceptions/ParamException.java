package com.dma.barannik.dmaflasher.Communication.Exceptions;

import com.dma.barannik.dmaflasher.Communication.DeviceParam;

public class ParamException extends CommandFormatException {
    private DeviceParam param;
    private String value;


    public ParamException(DeviceParam param, String value) {
        this.param = param;
        this.value = value;
    }

    public DeviceParam getParam() {
        return param;
    }

    public String getValue() {
        return value;
    }
}
