package com.dma.barannik.dmaflasher.Workflow;

import android.app.Activity;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ActivityLineCloser {
    private static Map<Integer, List<Activity>> lines = new HashMap();
    private static Map<Activity, Integer> activityContainedLines = new HashMap();

    public static void closeWhenEventsHappened(Activity toClose, int... events) {
        for (int event: events) {
            addActivityToLine(event, toClose);
            activityContainedLines.put(toClose, event);
        }
    }

    public static void eventHappened(int event) {
        List<Activity> line = lines.get(event);
        if (line != null) {
            for (Activity toClose: line) {
                if (!toClose.isFinishing()) {
                    toClose.finishActivity(1);
                    toClose.finish();
                }
                removeActivityFromAllLines(toClose);
            }
            line.clear();
        }
    }

    private static void addActivityToLine(int event, Activity activity) {
        List<Activity> line = lines.get(event);
        if (line == null) {
            line = new LinkedList<>();
            lines.put(event, line);
        }
        line.add(activity);
    }

    private static void removeActivityFromAllLines(Activity activity) {
        if (activityContainedLines.containsKey(activity)) {
            int eventKey = activityContainedLines.get(activity);
            if (lines.containsKey(eventKey)) {
                lines.get(eventKey).remove(activity);
            }
        }
    }
}
