package com.dma.barannik.dmaflasher.Communication.Connection;

import com.dma.barannik.dmaflasher.Communication.Exceptions.DeviceDisconnectedException;

public class IgnoreEchoConnection implements Connection {

    private Connection connection;

    public IgnoreEchoConnection(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void open() throws DeviceDisconnectedException {
        connection.open();
    }

    @Override
    public void close() throws DeviceDisconnectedException {
        connection.close();
    }

    @Override
    public void send(String data) throws DeviceDisconnectedException {
        connection.send(data);
    }

    @Override
    public String read(int lines) throws DeviceDisconnectedException {
        String data = connection.read(lines + 1);
        return data.substring(data.indexOf('\n') + 1);
    }
}
