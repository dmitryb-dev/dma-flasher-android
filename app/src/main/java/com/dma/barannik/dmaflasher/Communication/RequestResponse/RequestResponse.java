package com.dma.barannik.dmaflasher.Communication.RequestResponse;

import com.dma.barannik.dmaflasher.Communication.Exceptions.DeviceDisconnectedException;

public interface RequestResponse {
    String send(String request, int linesOfRequest) throws DeviceDisconnectedException;
}
