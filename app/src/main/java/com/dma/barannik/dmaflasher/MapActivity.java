package com.dma.barannik.dmaflasher;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.dma.barannik.dmaflasher.Communication.Protocol.Async.DMAParamsWriter;
import com.dma.barannik.dmaflasher.Communication.Protocol.DMA2DeviceProtocol;
import com.dma.barannik.dmaflasher.Persistance.DMAContext;
import com.dma.barannik.dmaflasher.View.AndroidMapView;

public class MapActivity extends AppCompatActivity {

    private static final int SYNCHRONIZATION_END_CODE = 99;
    private DMA2DeviceProtocol protocol;
    private AndroidMapView mapView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        Log.d("create", "activity");

  //      ActivityLineCloser.closeWhenEventsHappened(this,
    //            DMAContext.EVENT_CONNECTION_ERROR);

        protocol = DMAContext.protocol;

        DMAContext.loadedMap = DMAContext.selectedMap.load();

        final AndroidMapView mapView = new AndroidMapView(
                this,
                getSupportFragmentManager(),
                (ViewGroup) findViewById(R.id.root),
                DMAContext.loadedMap
        );

        ((TabLayout) findViewById(R.id.tabsLayout)).setupWithViewPager(mapView.getViewPager());

        findViewById(R.id.synchronize).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DMAContext.values = mapView.getValues();
                DMAContext.operationRequest = new DMAParamsWriter(protocol, DMAContext.values);
                Intent synchronizationActivity = new Intent(MapActivity.this, SynchronizationActivity.class);
                startActivityForResult(synchronizationActivity, SYNCHRONIZATION_END_CODE);
            }
        });

        findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mapView.invalidate();
                Intent saveActivity = new Intent(MapActivity.this, SaveActivity.class);
                startActivity(saveActivity);
            }
        });

        findViewById(R.id.files).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        this.mapView = mapView;
        if (DMAContext.values != null) {
            mapView.setValues(DMAContext.values);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (SYNCHRONIZATION_END_CODE == requestCode) {
            if (resultCode == SynchronizationActivity.STATUS_FAIL) {
                setResult(SynchronizationActivity.STATUS_FAIL);
                finish();
            }
        }
    }
}
