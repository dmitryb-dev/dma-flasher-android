package com.dma.barannik.dmaflasher.Communication.Protocol.Async;

import com.barannik.events.Event;

public interface AsyncOperationListener {
    void onStart(LongAsyncOperation operation);
    void onCancel(LongAsyncOperation operation);
    void onFail(Exception e);
    void onSuccess(LongAsyncOperation operation);
    void onProgress(Progress progress);
}
