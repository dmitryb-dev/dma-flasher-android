package com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.field;

public class FloatNumberField extends FieldWithId {

    private double defaultValue;
    private double min, max;

    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }

    public FloatNumberField() {
        max = Double.MAX_VALUE;
        min = Double.MIN_VALUE;
        defaultValue = 0;
    }

    public FloatNumberField(double min, double max) {
        this.min = min;
        this.max = max;

        if (min <= 0 && max >= 0) defaultValue = 0.;
        else defaultValue =  min > 0? min : max;
    }

    public FloatNumberField(double min, double max, double defaultValue) {
        this.min = min;
        this.max = max;
        this.defaultValue = defaultValue;
    }

    @Override
    public FloatNumberField clone() {
        return new FloatNumberField(min, max, defaultValue);
    }

    @Override
    public Object accept(FieldVisitor visitor) {
        return visitor.visit(this);
    }

    @Override
    public Double getDefaultValue() {
        return defaultValue;
    }

    @Override
    public void setDefaultValue(Object value) {
        defaultValue = (double) value;
    }
}
