package com.dma.barannik.dmaflasher;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.barannik.events.Listener;
import com.dma.barannik.dmaflasher.Communication.DeviceInfo;
import com.dma.barannik.dmaflasher.Communication.Protocol.Async.DMAParamsReader;
import com.dma.barannik.dmaflasher.Communication.Protocol.Async.LongAsyncOperation;
import com.dma.barannik.dmaflasher.ParamsMap.DMADevice;
import com.dma.barannik.dmaflasher.Persistance.DMAContext;
import com.dma.barannik.dmaflasher.Persistance.FilesMapsSource;
import com.dma.barannik.dmaflasher.Persistance.MapSummary;
import com.dma.barannik.dmaflasher.Workflow.ActivityLineCloser;
import com.dma.barannik.dmaflasher.Workflow.Alerts;

public class SelectPresetActivity extends BTDependedActivity {

    private LinearLayout readTo, presets;
    private static final int SYNCHRONIZATION_END_CODE = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_preset);

        readTo = (LinearLayout) findViewById(R.id.readTo);
        presets = (LinearLayout) findViewById(R.id.presets);
     //   ((TextView) findViewById(R.id.deviceName)).setText(DMAContext.deviceInfo.toString());
        setTitle(DMAContext.deviceInfo.toString());

        updateMapsView();
    }

    public void updateMapsView() {
        readTo.removeAllViews();
        presets.removeAllViews();
        DMAContext.maps = new FilesMapsSource(this).supportedMapsFor(new DMADevice(DMAContext.deviceInfo));

        for (MapSummary map: DMAContext.maps) {
            addMapInLayout(map, map.isPreset() ? presets : readTo);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (DMAContext.needsToRefresh) {
            DMAContext.needsToRefresh = false;
            updateMapsView();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DMAContext.clear();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (SYNCHRONIZATION_END_CODE == requestCode) {
            if (resultCode == SynchronizationActivity.STATUS_SUCCESS) {
                openMap();
            } else if (resultCode == SynchronizationActivity.STATUS_FAIL) {
                Alerts.error(R.string.cannot_connect, this);
                DMAContext.clear();
            }
        }
    }

    private void addMapInLayout(final MapSummary summary, ViewGroup target) {
        View mapSummary = getLayoutInflater().inflate(R.layout.maps_summary, null);
        ((TextView) mapSummary.findViewById(R.id.mapName)).setText(summary.getName());
        mapSummary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DMAContext.selectedMap = summary;
                if (summary.isPreset()) {
                    Log.i(this.getClass().getSimpleName(), "Opening preset: " + summary.getName());
                    DMAContext.values = null;
                    openMap();
                } else {
                    Log.i(this.getClass().getSimpleName(), "Opening map: " + summary.getName());
                    readValues();
                }
            }
        });
        target.addView(mapSummary);
    }

    private void openMap() {
        Intent mapActivity = new Intent(SelectPresetActivity.this, MapActivity.class);
        startActivity(mapActivity);
   //     startActivityForResult(mapActivity, SYNCHRONIZATION_END_CODE);
    }

    private void readValues() {
        final DMAParamsReader reader = new DMAParamsReader(DMAContext.protocol, DMAContext.selectedMap.load().getDeclaredParams());
        reader.onSuccess().subscribe(new Listener<LongAsyncOperation>() {
            @Override
            public void onEvent(LongAsyncOperation observable) {
                DMAContext.values = reader.getResult();
            }
        });
        DMAContext.operationRequest = reader;

        Intent synchronizationActivity = new Intent(SelectPresetActivity.this, SynchronizationActivity.class);
        startActivityForResult(synchronizationActivity, SYNCHRONIZATION_END_CODE);
    }
}
