package com.dma.barannik.dmaflasher.ParamsMap.Legacy.Serialize;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex {
    public static List<String> find(String source, String regex) {
        List<String> result = new ArrayList<>();
        Matcher matcher = Pattern.compile(regex, Pattern.MULTILINE).matcher(source);
        while (matcher.find()) result.add(matcher.group());
        return result;
    }
}
