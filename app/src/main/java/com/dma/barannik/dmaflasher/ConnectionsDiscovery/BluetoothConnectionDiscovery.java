package com.dma.barannik.dmaflasher.ConnectionsDiscovery;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import com.barannik.events.Event;
import com.barannik.events.ObservableEvent;

public class BluetoothConnectionDiscovery implements ConnectionDiscovery {

    private final BroadcastReceiver deviceFoundAction = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(BluetoothDevice.ACTION_FOUND)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                deviceFoundEvent.notifySubscribers(new DMABluetoothConnectible(device));
                Log.i("BT", "Found device: " + device.getName() + ": " + device.getAddress());
            }
        }
    };
    private final ObservableEvent<DMABluetoothConnectible> deviceFoundEvent = new ObservableEvent<>();
    private Context context;
    private BluetoothAdapter adapter;

    public BluetoothConnectionDiscovery(Context context, BluetoothAdapter adapter) {
        this.context = context;
        this.adapter = adapter;
    }

    @Override
    public Event<DMABluetoothConnectible> onFound() {
        return deviceFoundEvent;
    }

    @Override
    public void start() {
        context.registerReceiver(deviceFoundAction, new IntentFilter(BluetoothDevice.ACTION_FOUND));
        adapter.startDiscovery();
    }

    @Override
    public void stop() {
        context.unregisterReceiver(deviceFoundAction);
        adapter.cancelDiscovery();
    }
}
