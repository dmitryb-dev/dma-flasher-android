package com.dma.barannik.dmaflasher.Communication.Exceptions;

public class DeviceDisconnectedException extends DeviceCommunicationException {
    public DeviceDisconnectedException() {
    }

    public DeviceDisconnectedException(String detailMessage) {
        super(detailMessage);
    }

    public DeviceDisconnectedException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public DeviceDisconnectedException(Throwable throwable) {
        super(throwable);
    }
}
