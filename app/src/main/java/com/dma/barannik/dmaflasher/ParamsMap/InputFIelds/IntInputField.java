package com.dma.barannik.dmaflasher.ParamsMap.InputFIelds;

import com.dma.barannik.dmaflasher.ParamsMap.Exceprtions.WrongDefaultValueException;

public class IntInputField extends AbstractInputField<Integer> {
    private int min, max;

    public IntInputField(String name, String comment, int value, int min, int max) {
        super(name, comment, value);
   /*     if (value < min || value > max)
            throw new WrongDefaultValueException("Wrong value (" + value + "): min = " + min + ", max = " + max);*/
        this.min = min;
        this.max = max;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }
}
