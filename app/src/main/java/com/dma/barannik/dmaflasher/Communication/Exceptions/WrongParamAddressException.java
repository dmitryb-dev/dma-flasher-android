package com.dma.barannik.dmaflasher.Communication.Exceptions;

import com.dma.barannik.dmaflasher.Communication.DeviceParam;

public class WrongParamAddressException extends ParamException {

    public WrongParamAddressException(DeviceParam param, String value) {
        super(param, value);
    }
}
