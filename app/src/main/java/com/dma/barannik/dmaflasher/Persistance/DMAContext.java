package com.dma.barannik.dmaflasher.Persistance;

import com.dma.barannik.dmaflasher.Communication.Connection.Connection;
import com.dma.barannik.dmaflasher.Communication.DeviceInfo;
import com.dma.barannik.dmaflasher.Communication.DeviceParam;
import com.dma.barannik.dmaflasher.Communication.Protocol.Async.LongAsyncOperation;
import com.dma.barannik.dmaflasher.Communication.Protocol.DMA2DeviceProtocol;
import com.dma.barannik.dmaflasher.ConnectionsDiscovery.Connectible;
import com.dma.barannik.dmaflasher.ParamsMap.ParamsMap;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

public class DMAContext {

    public static int
            EVENT_CONNECTION_ERROR = 1,
            EVENT_SYNCHRONIZATION_CANCELED = 2;

    public static Connectible connected;
    public static DeviceInfo deviceInfo;
    public static Collection<MapSummary> maps;
    public static MapSummary selectedMap;
    public static ParamsMap loadedMap;
    public static DMA2DeviceProtocol protocol;
    public static Map<DeviceParam, String> values;
    public static LongAsyncOperation operationRequest;
    public static boolean needsToRefresh;
    public static Connection connection;

    static {
        clear();
    }

    public static void clear() {
        if (connection != null)
            connection.close();
        connected = null;
        deviceInfo = null;
        maps = Collections.EMPTY_LIST;
        selectedMap = null;
        loadedMap = null;
        protocol = null;
        values = null;
        connection = null;
        operationRequest = null;
        needsToRefresh = false;
    }
}
