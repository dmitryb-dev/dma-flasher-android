package com.dma.barannik.dmaflasher.ParamsMap.InputFIelds;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PresetInputField extends AbstractInputField<Integer> implements Iterable<PresetInputField.Value> {

    public PresetInputField(String name, String comment, List<PresetInputField.Value> values, int defaultValueIndex) {
        super(name, comment, defaultValueIndex);
        this.values = values;
    }

    private List<Value> values = new ArrayList<>();
    public Value getValue(int index) { return values.get(index); }

    public static class Value implements Serializable {
        private String name, value;

        public String getName() {
            return name;
        }

        public String getValue() {
            return value;
        }

        public Value(String name, String value) {
            this.name = name;
            this.value = value;
        }

        @Override
        public int hashCode() {
            int result = name.hashCode();
            result = 31 * result + value.hashCode();
            return result;
        }
    }

    public int getPositionOfValue(String value) {
        int i = 0;
        for (Value v: values) {
            if (v.value.equals(value))
                return i;
            i++;
        }
        return -1;
    }

    @Override
    public int hashCode() {
        return values.hashCode();
    }

    @Override
    public Iterator<Value> iterator() {
        return values.iterator();
    }
}
