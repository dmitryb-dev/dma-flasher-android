package com.dma.barannik.dmaflasher.Communication.Connection;

import android.util.Log;
import com.dma.barannik.dmaflasher.Communication.Exceptions.CannotConnectDeviceException;
import com.dma.barannik.dmaflasher.Communication.Exceptions.DeviceDisconnectedException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DMAConnection implements Connection {

    private static final byte[] END_OF_LINE = { 0x0D, 0x0A };
    private static final String CHARSET_NAME = "ASCII";
    private static final int MESSAGE_MAX_LENGTH_BYTES = 4096;

    private Socket socket;
    private InputStream in;
    private OutputStream out;

    public DMAConnection(Socket socket) throws CannotConnectDeviceException {
        this.socket = socket;
    }

    @Override
    public void open() throws DeviceDisconnectedException {
        try {
            socket.open();
            out = socket.getOutputStream();
            in = new InputStreamWithTimeout(socket.getInputStream(), 3000);
          //  in = socket.getInputStream();
        } catch (IOException e) {
            close();
            throw new CannotConnectDeviceException(e);
        }
    }

    @Override
    public void close() throws DeviceDisconnectedException {
        try {
            if (in != null) in.close();
            if (out != null) out.close();
            in = null;
            out = null;
            socket.close();
        } catch (IOException e) {
            throw new DeviceDisconnectedException(e);
        }
    }

    @Override
    public void send(String data) throws DeviceDisconnectedException {
        if (socket == null || out == null)
            throw new IllegalStateException("Connection not opened");

        Log.i("Sending", data);
        try {
            writeDataTo(out, data);
        } catch (IOException e) {
            throw new DeviceDisconnectedException(e);
        }
    }

    @Override
    public String read(int lines) throws DeviceDisconnectedException {
        if (socket == null || in == null)
            throw new IllegalStateException("Connection not opened");

        try {
            return new String(readLinesFrom(in, lines), CHARSET_NAME)
                    .replaceAll(new String(END_OF_LINE, CHARSET_NAME), "\n");
        } catch (InputStreamWithTimeout.TimeoutException e) {
            return null;
        } catch (IOException e) {
            throw new DeviceDisconnectedException(e);
        }
    }

    private static void writeDataTo(OutputStream out, String data) throws IOException {
        out.write(data.getBytes(CHARSET_NAME));
        out.write(END_OF_LINE);
        out.flush();
    }

    private static byte[] readLinesFrom(InputStream in, int lines) throws IOException {
        byte[] buffer = new byte[MESSAGE_MAX_LENGTH_BYTES];
        int offset = 0, read;
        while ((read = in.read(buffer, offset, MESSAGE_MAX_LENGTH_BYTES - offset)) != -1) {
            Log.d("Reading", String.format("Read: %s, offset: %s", read, offset));
            int indexOFEnd = ConnectionUtils.findLastOccurence(buffer, END_OF_LINE, lines);
            if (indexOFEnd >= 0)
                return ConnectionUtils.subArray(buffer, indexOFEnd);
            offset += read;
        }
        throw new IOException("Device hasn't sent enough lines of answer.");
    }
}
