package com.dma.barannik.dmaflasher.Communication.Exceptions;

public class DeviceCommunicationException extends RuntimeException {
    public DeviceCommunicationException() {
    }

    public DeviceCommunicationException(String detailMessage) {
        super(detailMessage);
    }

    public DeviceCommunicationException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public DeviceCommunicationException(Throwable throwable) {
        super(throwable);
    }
}
