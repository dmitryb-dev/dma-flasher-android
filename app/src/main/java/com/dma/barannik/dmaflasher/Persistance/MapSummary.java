package com.dma.barannik.dmaflasher.Persistance;

import com.dma.barannik.dmaflasher.ParamsMap.DMADevice;
import com.dma.barannik.dmaflasher.ParamsMap.ParamsMap;

public interface MapSummary {
    String getLanguage();
    String getName();
    DMADevice getTargetDevice();
    boolean isPreset();
    ParamsMap load();
}
