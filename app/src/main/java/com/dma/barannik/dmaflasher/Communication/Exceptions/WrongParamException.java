package com.dma.barannik.dmaflasher.Communication.Exceptions;

import com.dma.barannik.dmaflasher.Communication.DeviceParam;

public class WrongParamException extends ParamException {

    public WrongParamException(DeviceParam param, String value) {
        super(param, value);
    }
}
