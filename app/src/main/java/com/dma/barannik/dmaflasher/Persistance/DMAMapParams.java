package com.dma.barannik.dmaflasher.Persistance;

public class DMAMapParams {
    public static DMAMapParams fromFilename(String filename) {
        String[] dmaInfo = FileUtils.getNameWithoutExtension(filename).split("__");
        return new DMAMapParams(dmaInfo[1].replaceAll("_", " "), dmaInfo[0], dmaInfo[2]);
    }

    private String name, targetDevice, language;

    private DMAMapParams(String name, String targetDevice, String language) {
        this.name = name;
        this.targetDevice = targetDevice;
        this.language = language;
    }

    public String getName() {
        return name;
    }

    public String getTargetDevice() {
        return targetDevice;
    }

    public String getLanguage() {
        return language;
    }
}
