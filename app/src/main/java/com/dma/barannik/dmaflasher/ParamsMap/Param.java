package com.dma.barannik.dmaflasher.ParamsMap;

import com.dma.barannik.dmaflasher.ParamsMap.InputFIelds.InputField;

import java.io.Serializable;

public class Param implements Serializable {
    private String address, type;
    private InputField field;

    public Param(String address, String type, InputField field) {
        this.address = address;
        this.type = type;
        this.field = field;
    }

    public String getAddress() {
        return address;
    }

    public String getType() {
        return type;
    }

    public InputField getField() {
        return field;
    }
}
