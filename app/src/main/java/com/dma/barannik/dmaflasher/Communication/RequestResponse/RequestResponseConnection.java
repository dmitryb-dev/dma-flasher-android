package com.dma.barannik.dmaflasher.Communication.RequestResponse;

import com.dma.barannik.dmaflasher.Communication.Connection.Connection;
import com.dma.barannik.dmaflasher.Communication.Exceptions.DeviceDisconnectedException;

public class RequestResponseConnection implements RequestResponse{

    public RequestResponseConnection(Connection connection) {
        this.connection = connection;
    }

    public String send(String request, int linesOfResponse) throws DeviceDisconnectedException {
        connection.send(request);
        return connection.read(linesOfResponse).trim();
    }

    private Connection connection;
}
