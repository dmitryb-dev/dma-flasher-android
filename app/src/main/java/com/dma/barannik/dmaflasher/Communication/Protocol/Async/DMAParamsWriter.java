package com.dma.barannik.dmaflasher.Communication.Protocol.Async;

import com.dma.barannik.dmaflasher.Communication.DeviceParam;
import com.dma.barannik.dmaflasher.Communication.Protocol.DeviceProtocol;

import java.util.Iterator;
import java.util.Map;

public class DMAParamsWriter extends StepByStepAsyncOperation {

    private DeviceProtocol protocol;
    private Iterator<Map.Entry<DeviceParam, String>> values;
    private int valuesCount;

    public DMAParamsWriter(DeviceProtocol protocol, Map<DeviceParam, String> values) {
        this.protocol = protocol;
        this.values = values.entrySet().iterator();
        valuesCount = values.size();
    }

    @Override
    public int getStepsCount() {
        return valuesCount;
    }

    @Override
    public void onStep(int i) {
        if (!values.hasNext()) {
            return;
        }
        synchronized (protocol) {
            Map.Entry<DeviceParam, String> value = values.next();
            protocol.set(value.getKey(), value.getValue());
        }
    }
}
