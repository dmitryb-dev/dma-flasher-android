package com.dma.barannik.dmaflasher.ParamsMap.InputFIelds;

import com.dma.barannik.dmaflasher.ParamsMap.Exceprtions.WrongDefaultValueException;

public class StringInputField extends AbstractInputField<String> {
    private int minLength, maxLength;

    public StringInputField(String name, String comment, String value, int minLength, int maxLength) {
        super(name, comment, value);
    /*    if (value.length() < minLength || value.length() > maxLength)
            throw new WrongDefaultValueException(
                    "Wrong length of string \"\" + value + \"\"(" + value.length() + "): " +
                            "min length = " + minLength + ", max length = " + maxLength);*/
        this.minLength = minLength;
        this.maxLength = maxLength;
    }

    public StringInputField(String name, String comment, String value, int maxLength) {
        this(name, comment, value, 0, maxLength);
    }

    public StringInputField(String name, String comment, int maxLength) {
        this(name, comment, "", 0, maxLength);
    }

    public int getMinLength() {
        return minLength;
    }

    public int getMaxLength() {
        return maxLength;
    }

}
