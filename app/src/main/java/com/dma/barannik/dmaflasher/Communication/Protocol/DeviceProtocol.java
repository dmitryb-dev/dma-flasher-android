package com.dma.barannik.dmaflasher.Communication.Protocol;

import com.dma.barannik.dmaflasher.Communication.DeviceInfo;
import com.dma.barannik.dmaflasher.Communication.DeviceParam;
import com.dma.barannik.dmaflasher.Communication.Exceptions.DeviceCommunicationException;

import java.io.Serializable;

public interface DeviceProtocol {
    String getValue(DeviceParam param) throws DeviceCommunicationException;
    void set(DeviceParam param, String value) throws DeviceCommunicationException;
    DeviceInfo getInfo() throws DeviceCommunicationException;
}
