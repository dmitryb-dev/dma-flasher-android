package com.dma.barannik.dmaflasher.ParamsMap.Persistance.Serialize;

import com.dma.barannik.dmaflasher.ParamsMap.ParamsMap;

public interface ParamsMapToString {
    String toString(ParamsMap paramsMap);
}
