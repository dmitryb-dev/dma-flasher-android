package com.dma.barannik.dmaflasher.View;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import com.dma.barannik.dmaflasher.Workflow.Alerts;
import com.dma.barannik.dmaflasher.ParamsMap.InputFIelds.InputFieldVisitor;
import com.dma.barannik.dmaflasher.ParamsMap.InputFIelds.IntInputField;
import com.dma.barannik.dmaflasher.ParamsMap.InputFIelds.PresetInputField;
import com.dma.barannik.dmaflasher.ParamsMap.InputFIelds.StringInputField;
import com.dma.barannik.dmaflasher.R;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class ViewFactoryInputFieldVisitor implements InputFieldVisitor<AndroidParamView>, Serializable {

    private Context context;

    public ViewFactoryInputFieldVisitor(Context context) {
        this.context = context;
    }

    @Override
    public AndroidParamView visit(final IntInputField intInputField) {
        final EditText view = new EditText(context);
        view.setInputType(InputType.TYPE_CLASS_NUMBER);
     //   view.addTextChangedListener(new
        return new AndroidParamView(view, intInputField) {
            {
                setValue("" + intInputField.getDefaultValue());
            }
            @Override
            public String getValue() {
                return view.getText().toString().substring(0, Math.min(view.getText().length(), 9));
            }

            @Override
            public void setValue(String value) throws UnsupportedValueException {
                try {
                    intInputField.setDefaultValue(Integer.parseInt(value));
                    view.setText(value);
                    view.postInvalidate();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    @Override
    public AndroidParamView visit(final StringInputField stringInputField) {
        final EditText view = new EditText(context);
        view.setInputType(InputType.TYPE_CLASS_TEXT);
        return new AndroidParamView(view, stringInputField) {
            {
                setValue(stringInputField.getDefaultValue());
            }
            @Override
            public String getValue() {
                return view.getText().toString();
            }

            @Override
            public void setValue(String value) throws UnsupportedValueException {
                view.setText(value);
                view.postInvalidate();
                stringInputField.setDefaultValue(value);
            }
        };
    }

    @Override
    public AndroidParamView visit(final PresetInputField presetField) {
        final Spinner spinner = new Spinner(context);
        List<String> values = new LinkedList<>();
        for (PresetInputField.Value v: presetField)
            values.add(v.getValue() + ": " + v.getName());
        spinner.setAdapter(new ArrayAdapter<String>(context, R.layout.support_simple_spinner_dropdown_item, values));

        return new AndroidParamView(spinner, presetField) {
            {
                setValue("" + presetField.getValue(presetField.getDefaultValue()).getValue());
            }
            @Override
            public String getValue() {
                return presetField.getValue(spinner.getSelectedItemPosition()).getValue();
            }

            @Override
            public void setValue(String value) throws UnsupportedValueException {
                spinner.setSelection(presetField.getPositionOfValue(value));
                try {
                    presetField.setDefaultValue(presetField.getPositionOfValue(value));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private static class LimitedNumberField implements TextWatcher {

        private IntInputField field;
        private EditText owner;
        private Activity activity;

        public LimitedNumberField(IntInputField field, EditText owner) {
            this.field = field;
            this.owner = owner;
        }

        private CharSequence prevValue;

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            prevValue = s;
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {
                int value = Integer.parseInt(s.toString());

            } catch (NumberFormatException e) {
                Alerts.errorMessage(R.string.out_of_bound, activity);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.toString().trim().length() == 0) {
                owner.setText(field.getDefaultValue());
            }
        }
    }
}
