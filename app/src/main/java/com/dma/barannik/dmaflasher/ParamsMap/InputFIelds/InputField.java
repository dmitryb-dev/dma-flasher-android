package com.dma.barannik.dmaflasher.ParamsMap.InputFIelds;

import java.io.Serializable;

public interface InputField<T> extends Serializable {
    String getComment();
    String getName();
    T getDefaultValue();
    void setDefaultValue(T value);
}
