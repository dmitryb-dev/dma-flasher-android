package com.dma.barannik.dmaflasher.Communication.Protocol.Async;

import android.widget.TextView;
import com.barannik.events.Event;
import com.barannik.events.Listener;
import com.barannik.events.ObservableEvent;
import com.dma.barannik.dmaflasher.Persistance.DMAContext;
import com.dma.barannik.dmaflasher.R;
import com.dma.barannik.dmaflasher.Workflow.ActivityLineCloser;

public abstract class AbstractLongAsyncOperation implements LongAsyncOperation {

    protected ObservableEvent<LongAsyncOperation> onStart = new ObservableEvent<>();
    protected ObservableEvent<LongAsyncOperation> onCancel = new ObservableEvent<>();
    protected ObservableEvent<Exception> onFail = new ObservableEvent<>();
    protected ObservableEvent<LongAsyncOperation> onSuccess = new ObservableEvent<>();
    protected ObservableEvent<Progress> onProgress = new ObservableEvent<>();

    @Override
    public Event<LongAsyncOperation> onStart() {
        return onStart;
    }

    @Override
    public Event<LongAsyncOperation> onCancel() {
        return onCancel;
    }

    @Override
    public Event<Exception> onFail() {
        return onFail;
    }

    @Override
    public Event<LongAsyncOperation> onSuccess() {
        return onSuccess;
    }

    @Override
    public Event<Progress> onProgress() {
        return onProgress;
    }

    @Override
    public void subscribeToAllEvents(final AsyncOperationListener listener) {
        DMAContext.operationRequest.onCancel().subscribe(new Listener<LongAsyncOperation>() {
            @Override
            public void onEvent(LongAsyncOperation observable) {
                listener.onCancel(observable);
            }
        });
        DMAContext.operationRequest.onFail().subscribe(new Listener<Exception>() {
            @Override
            public void onEvent(Exception e) {
                listener.onFail(e);
            }
        });
        DMAContext.operationRequest.onStart().subscribe(new Listener<LongAsyncOperation>() {
            @Override
            public void onEvent(LongAsyncOperation observable) {
                listener.onStart(observable);
            }
        });
        DMAContext.operationRequest.onSuccess().subscribe(new Listener<LongAsyncOperation>() {
            @Override
            public void onEvent(LongAsyncOperation observable) {
                listener.onSuccess(observable);
            }
        });
        DMAContext.operationRequest.onProgress().subscribe(new Listener<Progress>() {
            @Override
            public void onEvent(Progress observable) {
                listener.onProgress(observable);
            }
        });
    }
}
