package com.dma.barannik.dmaflasher.View;

public class UnsupportedValueException extends RuntimeException {
    public UnsupportedValueException() {
        super();
    }

    public UnsupportedValueException(String detailMessage) {
        super(detailMessage);
    }

    public UnsupportedValueException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public UnsupportedValueException(Throwable throwable) {
        super(throwable);
    }
}
