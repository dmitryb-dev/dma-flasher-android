package com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.field;

public class BooleanField extends FieldWithId {

    public BooleanField clone() {
        return new BooleanField(defaultValue);
    }

    private boolean defaultValue;

    public BooleanField() {
        this.defaultValue = false;
    }

    public BooleanField(boolean defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public Object accept(FieldVisitor visitor) {
        return visitor.visit(this);
    }

    @Override
    public Boolean getDefaultValue() {
        return defaultValue;
    }

    @Override
    public void setDefaultValue(Object value) {
        defaultValue = (boolean) value;
    }
}
