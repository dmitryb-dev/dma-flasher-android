package com.dma.barannik.dmaflasher.Persistance;

import java.io.*;

public class FileUtils {
    public static String getExtension(String name) {
        int indexOfDot = name.lastIndexOf('.');
        return indexOfDot > 0? name.substring(indexOfDot + 1) : "";
    }
    public static String getExtension(File file) {
        return getExtension(file.getName());
    }

    public static String getNameWithoutExtension(String name) {
        int indexOfDot = name.lastIndexOf('.');
        return indexOfDot > 0? name.substring(0, indexOfDot) : name;
    }
    public static String getNameWithoutExtension(File file) {
        return getNameWithoutExtension(file.getName());
    }

    public static boolean isFileHasExtension(File file, String... extensions) {
        for (int i = 0; i < extensions.length; i++)
            if (getExtension(file).equals(extensions[i]))
                return true;
        return false;
    }
    public static boolean isFileHasExtension(String file, String... extensions) {
        for (int i = 0; i < extensions.length; i++)
            if (getExtension(file).equals(extensions[i]))
                return true;
        return false;
    }

    public static String readFile(File file) {
        try {
            return readStream(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(String.format("Can't open file $s", file.getName()), e);
        }
    }
    public static String readStream(InputStream stream) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(stream, "windows-1251"));
            StringBuilder stringBuilder = new StringBuilder("");

            String line;
            while ((line = reader.readLine()) != null)
                stringBuilder.append(line).append("\n");

            return stringBuilder.toString();
        } catch (IOException e) {
            throw new IllegalStateException("Can't read file", e);
        } finally {
            try {
                reader.close();
            } catch (IOException e) {}
        }
    }
}
