package com.dma.barannik.dmaflasher.ParamsMap.Legacy.Serialize;

import com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.Schema;

public interface SchemaParser {

    Schema parse(String source);

}
