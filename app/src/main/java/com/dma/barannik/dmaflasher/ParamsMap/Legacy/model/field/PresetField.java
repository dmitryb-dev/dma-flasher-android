package com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.field;

import com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.Value;

import java.util.ArrayList;
import java.util.Iterator;

public class PresetField extends FieldWithId implements Iterable<Value> {

    private int defaultField;
    private ArrayList<Value> values = new ArrayList<>();

    public Value get(int number) {
        return number < values.size()? values.get(number) : null;
    }

    public void addValue(Value v) {
        values.add(v);
    }
    public void removeValue(Value v) {
        values.remove(v);
    }

    public int getValueIndex(Object value) {
        int i = 0;
        for (Value v : values) {
            if (v.getValue().equals(value))
                return i;
            i++;
        }
        return -1;
    }

    public void setDefaultField(int index) {
        if (index >= 0 && index < values.size()) defaultField = index;
    }

    public int getDefaultField() {
        return defaultField;
    }

    @Override
    public Iterator<Value> iterator() {
        return values.iterator();
    }

    @Override
    public Field clone() {
        PresetField clone = new PresetField();
        for (Value v : values)
            clone.addValue(v.clone());
        clone.defaultField = defaultField;
        return clone;
    }

    @Override
    public Object accept(FieldVisitor visitor) {
        return visitor.visit(this);
    }

    @Override
    public Integer getDefaultValue() {
        return defaultField;
    }

    @Override
    public void setDefaultValue(Object value) {
        defaultField = getValueIndex(value);
    }
}
