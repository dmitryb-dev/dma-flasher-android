package com.dma.barannik.dmaflasher;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.barannik.events.Listener;
import com.dma.barannik.dmaflasher.Communication.DeviceParam;
import com.dma.barannik.dmaflasher.Communication.Exceptions.DeviceDisconnectedException;
import com.dma.barannik.dmaflasher.Communication.Exceptions.DeviceInternalErrorException;
import com.dma.barannik.dmaflasher.Communication.Exceptions.WrongParamAddressException;
import com.dma.barannik.dmaflasher.Communication.Exceptions.WrongParamException;
import com.dma.barannik.dmaflasher.Communication.Exceptions.WrongParamTypeException;
import com.dma.barannik.dmaflasher.Communication.Protocol.Async.AbstractLongAsyncOperation;
import com.dma.barannik.dmaflasher.Communication.Protocol.Async.AsyncOperationListener;
import com.dma.barannik.dmaflasher.Communication.Protocol.Async.LongAsyncOperation;
import com.dma.barannik.dmaflasher.Communication.Protocol.Async.Progress;
import com.dma.barannik.dmaflasher.ParamsMap.Exceprtions.WrongDefaultValueException;
import com.dma.barannik.dmaflasher.ParamsMap.Param;
import com.dma.barannik.dmaflasher.Persistance.DMAContext;
import com.dma.barannik.dmaflasher.Workflow.ActivityLineCloser;
import com.dma.barannik.dmaflasher.Workflow.Alerts;

import java.util.concurrent.atomic.AtomicInteger;

public class SynchronizationActivity extends Activity implements AsyncOperationListener {

    public static final int STATUS_SUCCESS = 777;
    public static final int STATUS_FAIL = 222;
    public static final int STATUS_CANCELED = 999;

    private static TextView status;
    private static ProgressBar progressBar;
    private final AtomicInteger lastExecutedStepNumber = new AtomicInteger(0);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync);

        status = (TextView) findViewById(R.id.status);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DMAContext.operationRequest.cancel();
            }
        });

        DMAContext.operationRequest.subscribeToAllEvents(this);
        DMAContext.operationRequest.start();
    }

    @Override
    public void onStart(LongAsyncOperation operation) {
        status.setText(R.string.connected);
    }

    @Override
    public void onCancel(LongAsyncOperation operation) {
        setResult(STATUS_CANCELED);
        finish();
    }

    @Override
    public void onFail(Exception ex) {
        try {
            throw ex;
        } catch (WrongParamAddressException e) {
            Alerts.errorMessage(
                    createErrorMessage(R.string.error_wrong_address, e.getParam(), lastExecutedStepNumber.get() + 1, e.getValue()),
                    this);
        } catch (WrongParamTypeException e) {
            Alerts.errorMessage(
                    createErrorMessage(R.string.error_wrong_type, e.getParam(), lastExecutedStepNumber.get() + 1, e.getValue()),
                    this);
        } catch (WrongParamException e) {
            Alerts.errorMessage(
                    createErrorMessage(R.string.error_wrong_value, e.getParam(), lastExecutedStepNumber.get() + 1, e.getValue()),
                    this);
        } catch (DeviceInternalErrorException e) {
            Alerts.errorMessage(R.string.error_timeout, this);
        } catch (DeviceDisconnectedException e) {
            Alerts.errorMessage(R.string.error_disconnected, this);
            setResult(STATUS_FAIL);
            finish();
        } catch (Throwable e) {
            setResult(STATUS_FAIL);
            finish();
        }
    }

    @Override
    public void onSuccess(LongAsyncOperation operation) {
        setResult(STATUS_SUCCESS);
        finish();
    }

    @Override
    public void onProgress(final Progress progress) {
        this.lastExecutedStepNumber.set(progress.getValue());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                status.setText(progress.getValue() + " / " + progress.getMax());
                progressBar.setMax(progress.getMax());
                progressBar.setProgress(progress.getValue());
            }
        });
    }

    @SuppressLint("DefaultLocale")
    private String createErrorMessage(int resource, DeviceParam param, int stepNumber, String value) {
        return String.format("%s %d: %s:%s = %s",
                getResources().getString(resource),
                stepNumber + 1,
                param.getAddress(),
                param.getType(),
                value
        );
    }
}
