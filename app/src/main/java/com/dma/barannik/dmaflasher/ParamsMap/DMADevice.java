package com.dma.barannik.dmaflasher.ParamsMap;

import com.dma.barannik.dmaflasher.Communication.DeviceInfo;

public class DMADevice {
    private String name;

    public String getName() {
        return name;
    }

    public DMADevice(String name) {
        this.name = name;
    }

    public DMADevice(DeviceInfo info) {
        this(info.getName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DMADevice dmaDevice = (DMADevice) o;

        return name.equals(dmaDevice.name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
