package com.dma.barannik.dmaflasher.ParamsMap.Persistance.Parsers;

import com.dma.barannik.dmaflasher.ParamsMap.ParamsMap;

public interface ParamsMapParser {
    ParamsMap parse(String source);
}
