package com.dma.barannik.dmaflasher.ParamsMap.InputFIelds;

public class AbstractInputField<T> implements InputField<T> {
    private String comment, name;
    private T value;

    public AbstractInputField(String name, String comment, T value) {
        this.comment = comment;
        this.value = value;
        this.name = name;
    }

    public AbstractInputField(T value) {
        this.value = value;
    }

    @Override
    public String getComment() {
        return comment;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public T getDefaultValue() {
        return value;
    }

    @Override
    public void setDefaultValue(T value) {
        this.value = value;
    }
}
