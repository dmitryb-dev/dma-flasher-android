package com.dma.barannik.dmaflasher.Communication.Exceptions;

public class DeviceInternalErrorException extends DeviceCommunicationException {
    public DeviceInternalErrorException() {
    }

    public DeviceInternalErrorException(String detailMessage) {
        super(detailMessage);
    }

    public DeviceInternalErrorException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public DeviceInternalErrorException(Throwable throwable) {
        super(throwable);
    }
}
