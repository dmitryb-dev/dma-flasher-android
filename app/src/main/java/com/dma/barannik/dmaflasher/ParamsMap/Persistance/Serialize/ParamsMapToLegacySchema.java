package com.dma.barannik.dmaflasher.ParamsMap.Persistance.Serialize;

import com.dma.barannik.dmaflasher.ParamsMap.InputFIelds.*;
import com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.Group;
import com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.Schema;
import com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.Value;
import com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.field.Field;
import com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.field.NumberField;
import com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.field.PresetField;
import com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.field.StringField;
import com.dma.barannik.dmaflasher.ParamsMap.Param;
import com.dma.barannik.dmaflasher.ParamsMap.ParamsMap;
import com.dma.barannik.dmaflasher.ParamsMap.Tab;

public class ParamsMapToLegacySchema {
    public static Schema convert(ParamsMap paramsMap) {
        Schema schema = new Schema(
                paramsMap.getTargetDevice().getName(),
                paramsMap.getName());
        schema.setSpeed(115200);

        for (Tab tab: paramsMap) {
            Group group = new Group(tab.getName());
            for (Param param: tab) {
                group.addParam(new com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.Param(
                    param.getField().getName(),
                    param.getAddress(),
                    param.getType(),
                    InputFieldAcceptor.accept(param.getField(), new FiledConverter()),
                    param.getField().getComment()
                ));
            }
            schema.addGroup(group);
        }
        return schema;
    }

    private static class FiledConverter implements InputFieldVisitor<Field> {
        @Override
        public Field visit(IntInputField intInputField) {
            return new NumberField(
                    intInputField.getMin(),
                    intInputField.getMax(),
                    intInputField.getDefaultValue());
        }

        @Override
        public Field visit(StringInputField stringInputField) {
            return new StringField(
                    stringInputField.getDefaultValue(),
                    stringInputField.getMaxLength()
            );
        }

        @Override
        public Field visit(PresetInputField presetField) {
            PresetField preset = new PresetField();
            for (PresetInputField.Value value: presetField)
                preset.addValue(new Value(value.getName(), value.getValue()));
            preset.setId(Integer.toString(presetField.hashCode()));
            preset.setDefaultField(presetField.getDefaultValue());
            return preset;
        }
    }
}
