package com.dma.barannik.dmaflasher.Communication.Connection;

import com.dma.barannik.dmaflasher.Communication.Exceptions.DeviceDisconnectedException;

public class SlowConnection implements Connection {

    private Connection connection;

    public SlowConnection(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void open() throws DeviceDisconnectedException {
        pause();
        connection.open();
    }

    @Override
    public void close() throws DeviceDisconnectedException {
        pause();
        connection.close();
    }

    @Override
    public void send(String data) throws DeviceDisconnectedException {
        pause();
        connection.send(data);
    }

    @Override
    public String read(int lines) throws DeviceDisconnectedException {
        pause();
        return connection.read(lines);
    }

    private void pause() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
