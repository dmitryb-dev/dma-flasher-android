package com.dma.barannik.dmaflasher.View;

import com.dma.barannik.dmaflasher.Communication.DeviceParam;

import java.util.Map;

public interface DeviceValues {
    void setValues(Map<DeviceParam, String> values);
    Map<DeviceParam, String> getValues();
}
