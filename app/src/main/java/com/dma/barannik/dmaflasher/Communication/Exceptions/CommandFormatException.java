package com.dma.barannik.dmaflasher.Communication.Exceptions;

public class CommandFormatException extends DeviceCommunicationException {
    public CommandFormatException() {
    }

    public CommandFormatException(String detailMessage) {
        super(detailMessage);
    }

    public CommandFormatException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public CommandFormatException(Throwable throwable) {
        super(throwable);
    }
}
