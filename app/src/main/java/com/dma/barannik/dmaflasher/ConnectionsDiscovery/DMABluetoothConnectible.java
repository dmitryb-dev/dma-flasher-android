package com.dma.barannik.dmaflasher.ConnectionsDiscovery;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import com.dma.barannik.dmaflasher.Communication.Connection.Connection;
import com.dma.barannik.dmaflasher.Communication.Connection.DMAConnection;
import com.dma.barannik.dmaflasher.Communication.Connection.LoggedConnection;
import com.dma.barannik.dmaflasher.Communication.Connection.SocketBluetoothSocketAdapter;
import com.dma.barannik.dmaflasher.Communication.Exceptions.CannotConnectDeviceException;

import java.util.UUID;

public class DMABluetoothConnectible implements Connectible {

    private BluetoothDevice device;

    public DMABluetoothConnectible(BluetoothDevice device) {
        this.device = device;
    }

    @Override
    public Connection connect() {
        try {
            UUID id = device.getUuids()[0].getUuid();
            BluetoothSocket bluetoothSocket = device.createRfcommSocketToServiceRecord(id);
            return new LoggedConnection(new DMAConnection(
                    new SocketBluetoothSocketAdapter(bluetoothSocket)
            ));
        } catch (Throwable e) {
            throw new CannotConnectDeviceException(e);
        }
    }

    public BluetoothDevice getDevice() {
        return device;
    }
}
