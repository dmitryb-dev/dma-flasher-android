package com.dma.barannik.dmaflasher.ParamsMap.Persistance.Serialize;

import com.dma.barannik.dmaflasher.ParamsMap.Legacy.Serialize.SchemaCreatorJSON;
import com.dma.barannik.dmaflasher.ParamsMap.ParamsMap;

public class PresetToString implements ParamsMapToString {

    @Override
    public String toString(ParamsMap paramsMap) {
        return new SchemaCreatorJSON().create(ParamsMapToLegacySchema.convert(paramsMap));
    }
}
