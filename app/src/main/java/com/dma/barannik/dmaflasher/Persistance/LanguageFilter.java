package com.dma.barannik.dmaflasher.Persistance;

import java.util.*;

public class LanguageFilter {
    public static Collection<MapSummary> filterByCurrentLanguage(Collection<MapSummary> maps) {
        List<MapSummary> result = new ArrayList<>();

        Map<String, List<MapSummary>> nameMaps = new HashMap();
        List<String> languages = new ArrayList<>();
        for (MapSummary summary: maps) {
            if (summary.getLanguage().equals("user")) {
                result.add(summary);
                continue;
            }

            String name = summary.getTargetDevice() + summary.getName() + summary.isPreset();
            if (nameMaps.get(name) == null) {
                nameMaps.put(name, new ArrayList<MapSummary>());
            }
            nameMaps.get(name).add(summary);
        }

        String systemLanguage = Locale.getDefault().getLanguage();
        String targetLanguage = languages.indexOf(systemLanguage) >= 0? systemLanguage : "en";

        for (List<MapSummary> summaries: nameMaps.values()) {
            MapSummary found = summaries.get(0);
            for (MapSummary summary: summaries) {
                if (summary.getLanguage().equals(targetLanguage) || summary.getLanguage().equals("en")) {
                    found = summary;
                    if (found.getLanguage().equals(targetLanguage))
                        break;
                }
            }
            result.add(found);
        }

        return result;
    }
}
