package com.dma.barannik.dmaflasher.ParamsMap.Legacy.model.field;

public interface Field {
    Field clone();
    Object accept(FieldVisitor visitor);
    Object getDefaultValue();
    void setDefaultValue(Object value);
}
