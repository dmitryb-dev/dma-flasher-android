package com.dma.barannik.dmaflasher.Communication.Protocol.Async;

import android.util.Log;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class StepByStepAsyncOperation extends AbstractLongAsyncOperation {

    private AtomicBoolean isCanceled = new AtomicBoolean(false);

    @Override
    public void start() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                onStart.notifySubscribers(StepByStepAsyncOperation.this);
                for (int i = 0; i < getStepsCount(); i++) {
                    if (isCanceled.get()) {
                        onCancel.notifySubscribers(StepByStepAsyncOperation.this);
                        return;
                    }
                    try {
                        onStep(i);
                        onProgress.notifySubscribers(new Progress(i, getStepsCount()));
                    } catch (Exception e) {
                        Log.e("Error", "Error during step", e);
                        onFail.notifySubscribers(e);
                        return;
                    }
                }
                onSuccess.notifySubscribers(StepByStepAsyncOperation.this);
            }
        }, "Async dma operation").start();
    }

    @Override
    public void cancel() {
        isCanceled.set(true);
    }

    abstract public int getStepsCount();
    abstract public void onStep(int i);
}
