package com.dma.barannik.dmaflasher.Persistance;

import com.dma.barannik.dmaflasher.ParamsMap.DMADevice;

import java.util.Collection;

public interface MapsSource {
    Collection<MapSummary> supportedMapsFor(DMADevice device);
}
