package com.dma.barannik.dmaflasher;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import com.dma.barannik.dmaflasher.Workflow.Alerts;

public class BTDependedActivity extends AppCompatActivity {

    private BluetoothAdapter adapter;
    private static final int REQUEST_ENABLE_BT = 1122;
    private static final int REQUEST_PERMISSIONS = 2211;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_PERMISSIONS);
        adapter = BluetoothAdapter.getDefaultAdapter();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ENABLE_BT && requireBluetooth()) {
            recreate();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSIONS: {
                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    onPermissionsFail();
                }
            }
        }
    }

    public boolean requireBluetooth() {
        if (adapter == null)
            Alerts.fatal(R.string.not_support_bt, this);
        if (adapter != null && !adapter.isEnabled())
            showEnableBt();
        return adapter != null && adapter.isEnabled();
    }

    public BluetoothAdapter getBluetoothAdapter() {
        return adapter;
    }

    private void showEnableBt() {
        Alerts.fixOrExit(R.string.enable_bt, R.string.turn_on, this, new Alerts.Fixer() {
            @Override
            public void fix() {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        });
    }

    protected void onPermissionsFail() {

    }
}
