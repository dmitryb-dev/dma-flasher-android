package com.dma.barannik.dmaflasher.Persistance;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;

import static org.junit.Assert.*;

public class FileUtilsTest {

    private static File fileMock(String fileName) {
        File file = Mockito.mock(File.class);
        Mockito.when(file.getName())
                .thenReturn(fileName);
        return file;
    }

    @Test
    public void isFileHasExtension() throws Exception {
        Assert.assertEquals("", FileUtils.getExtension(fileMock("ext0")));
        Assert.assertEquals("", FileUtils.getExtension(fileMock(".ext1")));
        Assert.assertEquals("ext2", FileUtils.getExtension(fileMock("n.ext2")));
        Assert.assertEquals("ext3", FileUtils.getExtension(fileMock("n1.ext3")));
        Assert.assertEquals("ext4", FileUtils.getExtension(fileMock("n1.n2.ext4")));

        Assert.assertFalse(FileUtils.isFileHasExtension(fileMock("n1.n2.n3.ext5"), "ext2", "ext3", "ext4", "ext6"));
        Assert.assertTrue(FileUtils.isFileHasExtension(fileMock("n1.n2.n3.ext5"), "ext2", "ext3", "ext4", "ext5"));
    }
}