package com.dma.barannik.dmaflasher;

import org.junit.Assert;

public class TestUtils {
    public static boolean isThrows(RunnableWithException method, Class<? extends Exception> expected) {
        try {
            method.run();
        } catch (Exception e) {
            return expected.isAssignableFrom(e.getClass());
        }
        return false;
    }

    public static void assertThrows(RunnableWithException method, Class<? extends Exception> expected) {
        Assert.assertTrue(isThrows(method, expected));
    }

    public interface RunnableWithException {
        void run() throws Exception;
    }
}
