package com.dma.barannik.dmaflasher.Persistance;

import org.mockito.Mockito;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

public class FileMocks {
    public static File file(String name) {
        File file = Mockito.mock(File.class);
        Mockito.when(file.isFile()).thenReturn(true);
        Mockito.when(file.isDirectory()).thenReturn(false);
        Mockito.when(file.getName()).thenReturn(name);
        return file;
    }

    public static File directory(String name, String... files) {
        Collection<File> filesInDirectory = new ArrayList<>();
        for (String fileName: files) {
            filesInDirectory.add(file(fileName));
        }

        return directory(name, filesInDirectory.toArray(new File[0]));
    }

    public static File directory(String name, File... files) {
        File directory = Mockito.mock(File.class);
        Mockito.when(directory.isFile()).thenReturn(false);
        Mockito.when(directory.isDirectory()).thenReturn(true);
        Mockito.when(directory.getName()).thenReturn(name);
        Mockito.when(directory.listFiles()).thenReturn(files);
        return directory;
    }
}
