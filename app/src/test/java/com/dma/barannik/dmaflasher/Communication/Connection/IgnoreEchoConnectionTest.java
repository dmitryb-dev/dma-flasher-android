package com.dma.barannik.dmaflasher.Communication.Connection;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class IgnoreEchoConnectionTest {
    @Test
    public void read() throws Exception {
        Connection connectionMock = Mockito.mock(Connection.class);
        Mockito.when(connectionMock.read(2))
                .thenReturn("echo\npayload");

        Connection ignoreEcho = new IgnoreEchoConnection(connectionMock);
        Assert.assertEquals("payload", ignoreEcho.read(1));
    }

}