package com.dma.barannik.dmaflasher.Communication.Connection;

import com.dma.barannik.dmaflasher.Communication.Exceptions.DeviceDisconnectedException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.*;

public class DMAConnectionTest {

    private final String END_OF_LINE_STRING = new String(new byte[] { 0x0D, 0x0A }, "ASCII");
    private final String MESSAGE =
            "line1" + END_OF_LINE_STRING +
            "line2" + END_OF_LINE_STRING +
            "line3" + END_OF_LINE_STRING +
            "unreachable";

    private Socket socketMock;

    public DMAConnectionTest() throws UnsupportedEncodingException {
    }

    @Before
    public void initSocket() throws IOException {
        InputStream inputStream = new ByteArrayInputStream(MESSAGE.getBytes());
        socketMock = Mockito.mock(Socket.class);
        Mockito.when(socketMock.getInputStream())
                .thenReturn(inputStream);
    }

    @Test
    public void send() throws Exception {
        OutputStream stream = new ByteArrayOutputStream(16);
        Mockito.when(socketMock.getOutputStream())
                .thenReturn(stream);

        final DMAConnection connection = new DMAConnection(socketMock);
        connection.open();

        final String MESSAGE_TO_SEND = "message";
        connection.send(MESSAGE_TO_SEND);

        Assert.assertEquals(MESSAGE_TO_SEND + END_OF_LINE_STRING, stream.toString());
    }

    @Test
    public void read1Line() throws Exception {
        final DMAConnection connection = new DMAConnection(socketMock);
        connection.open();

        String read = connection.read(1);
        Assert.assertEquals("line1", read);
    }
    @Test
    public void read2Lines() throws Exception {
        final DMAConnection connection = new DMAConnection(socketMock);
        connection.open();

        String read = connection.read(2);
        Assert.assertEquals("line1\nline2", read);
    }
    @Test
    public void read3Lines() throws Exception {
        final DMAConnection connection = new DMAConnection(socketMock);
        connection.open();

        String read = connection.read(3);
        Assert.assertEquals("line1\nline2\nline3", read);
    }
    @Test(expected = DeviceDisconnectedException.class)
    public void read4Lines() throws Exception {
        final DMAConnection connection = new DMAConnection(socketMock);
        connection.open();
        connection.read(4);
    }
}