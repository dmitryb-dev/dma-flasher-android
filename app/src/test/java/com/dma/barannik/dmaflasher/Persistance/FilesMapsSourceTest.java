package com.dma.barannik.dmaflasher.Persistance;

import android.content.Context;
import com.dma.barannik.dmaflasher.ParamsMap.DMADevice;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;
import java.util.Collection;

public class FilesMapsSourceTest {

    private Context contextMock = Mockito.mock(Context.class);

    private void assertMapsEquals(String targetDevice, String... expectedNames) {
        Collection<MapSummary> maps =
                new FilesMapsSource(contextMock)
                        .supportedMapsFor(new DMADevice(targetDevice));

        int i = 0;
        for (MapSummary summary: maps) {
            Assert.assertEquals(new DMADevice(targetDevice), summary.getTargetDevice());
            Assert.assertEquals(expectedNames[i++], summary.getName());
        }
    }

    @Before
    public void initContext() {
        File root = FileMocks.directory(
                "ROOT",
                FileMocks.directory("EMAP", "DMA1_file1.emap", "DMA1_file2.emap", "DMA1_file3.emap", "DMA2_file1.emap"),
                FileMocks.directory("DMASET", "DMA1_pfile1.dmaset", "DMA2_pfile1.dmaset", "DMA2_pfile2.dmaset", "DMA3_pfile1.bin"));

        Mockito.when(contextMock.getFilesDir())
                .thenReturn(root);
    }

    @Test
    public void supportedMapsForDMA1() throws Exception {
        assertMapsEquals("DMA1", "file1", "file2", "file3", "pfile1");
    }

    @Test
    public void supportedMapsForDMA2() throws Exception {
        assertMapsEquals("DMA2", "file1", "pfile1", "pfile2");
    }

    @Test
    public void supportedMapsForNotExistent() throws Exception {
        assertMapsEquals("DMA3", new String[]{});
    }

}