package com.dma.barannik.dmaflasher.Communication;

import com.dma.barannik.dmaflasher.Communication.Connection.Connection;
import com.dma.barannik.dmaflasher.Communication.Exceptions.DeviceDisconnectedException;
import com.dma.barannik.dmaflasher.Communication.Exceptions.WrongParamAddressException;
import com.dma.barannik.dmaflasher.Communication.Exceptions.WrongParamException;
import com.dma.barannik.dmaflasher.Communication.Exceptions.WrongParamTypeException;
import com.dma.barannik.dmaflasher.Communication.Protocol.DMA2DeviceProtocol;
import com.dma.barannik.dmaflasher.TestUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class DMA2DeviceProtocolTest {

    private DeviceParam paramMock = new DeviceParam("Doesn't", "matter");
    private Connection connectionMock;
    private DMA2DeviceProtocol protocol;
    @Before
    public void prepareConnectionMock() {
        this.connectionMock = Mockito.mock(Connection.class);
        this.protocol = new DMA2DeviceProtocol(connectionMock);
    }

    @Test
    public void getValue() throws Exception {
        Mockito.when(connectionMock.read(Mockito.anyInt())).thenReturn("answer");

        Assert.assertEquals("answer", protocol.getValue(paramMock));
        Mockito.verify(connectionMock).send(Mockito.eq("AT+RD=Doesn't,matter"));
        Mockito.verify(connectionMock).read(Mockito.eq(1));
    }

    @Test
    public void getValueWrongParam() {
        Mockito.when(connectionMock.read(Mockito.anyInt()))
                .thenReturn("#ERR ADRES")
                .thenReturn("#ERR TYPE")
                .thenThrow(new DeviceDisconnectedException());

        TestUtils.assertThrows(new TestUtils.RunnableWithException() {
            @Override
            public void run() throws Exception {
                protocol.getValue(paramMock);
            }
        }, WrongParamAddressException.class);
        TestUtils.assertThrows(new TestUtils.RunnableWithException() {
            @Override
            public void run() throws Exception {
                protocol.getValue(paramMock);
            }
        }, WrongParamTypeException.class);
        TestUtils.assertThrows(new TestUtils.RunnableWithException() {
            @Override
            public void run() throws Exception {
                protocol.getValue(paramMock);
            }
        }, DeviceDisconnectedException.class);
    }

    @Test
    public void set() throws Exception {
        Mockito.when(connectionMock.read(Mockito.anyInt())).thenReturn("Doesn't matter, but certanly not errorMessage");

        protocol.set(paramMock, "value");
        Mockito.verify(connectionMock).send(Mockito.eq("AT+WR=Doesn't,matter,value"));
        Mockito.verify(connectionMock).read(Mockito.eq(1));
    }

    @Test
    public void setValueWrongParam() {
        Mockito.when(connectionMock.read(Mockito.anyInt()))
                .thenReturn("#ERR ADRES")
                .thenReturn("#ERR TYPE")
                .thenReturn("#ERR PARAMETR")
                .thenThrow(new DeviceDisconnectedException());

        TestUtils.assertThrows(new TestUtils.RunnableWithException() {
            @Override
            public void run() throws Exception {
                protocol.set(paramMock, "a");
            }
        }, WrongParamAddressException.class);
        TestUtils.assertThrows(new TestUtils.RunnableWithException() {
            @Override
            public void run() throws Exception {
                protocol.set(paramMock, "b");
            }
        }, WrongParamTypeException.class);
        TestUtils.assertThrows(new TestUtils.RunnableWithException() {
            @Override
            public void run() throws Exception {
                protocol.set(paramMock, "c");
            }
        }, WrongParamException.class);
        TestUtils.assertThrows(new TestUtils.RunnableWithException() {
            @Override
            public void run() throws Exception {
                protocol.set(paramMock, "d");
            }
        }, DeviceDisconnectedException.class);
    }

    @Test
    public void getInfo() throws Exception {
        final String deviceName = "My name is Ron", deviceVersion = "Version 1.1.1.1.1";
        Mockito.when(connectionMock.read(Mockito.anyInt()))
                .thenReturn(deviceName)
                .thenReturn(deviceVersion);

        final DeviceInfo info = protocol.getInfo();
        Assert.assertEquals(deviceName, info.getName());
        Assert.assertEquals(deviceVersion, info.getVersion());
    }

    @Test(expected = DeviceDisconnectedException.class)
    public void getInfoBrokenConnection() throws Exception {
        Mockito.when(connectionMock.read(Mockito.anyInt()))
                .thenThrow(new DeviceDisconnectedException());

        protocol.getInfo();
    }
}